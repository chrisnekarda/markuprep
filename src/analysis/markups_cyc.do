// markups_cyc.do
//
// Results reported in table 1: Unconditional cyclicality
//
// "The Cyclical Behavior of the Price-Cost Markup"
// By Christopher J. Nekarda and Valerie A. Ramey
//
// See instructions in project README.
//
// -------------------------------------------------------------------------- //

set more off

noi disp as text "$S_DATE $S_TIME  begin markups_cyc.do"

if "$fil" != "hp" & "$fil" != "bk" & "$fil" != "fd" {
   noi disp as error "Must specify a cyclical filter. See analysis.do"
   exit
}

use ../../data/processed/markupcyc_data.dta, clear


local varlist = "lrgdp lmu_cd lmu_cd_ws lmu_cd_ws1 lmu_cd_ws_oh lmu_ces_yl lmu_ces_yl_zsvar lmu_ces_yk lmu_ces_yk_ums lmu_ces_yk_ujf lmu_ces_oh_yl lmu_ces_oh_yl_zsvar lmu_ces_oh_yk lmu_ces_oh_yk_ums lmu_ces_oh_yk_ujf lmu_cd_marg"

noi disp as text "$S_DATE $S_TIME    extracting cyclical components (using $fil filter) for:"
foreach xx of local varlist {
   noi disp as text "$S_DATE $S_TIME    `xx' "
   if "$fil"=="hp" | "$fil"=="bk" {
      tsfilter $fil `xx'_cyc = `xx'
   }
   if "$fil"=="fd" {
      gen `xx'_cyc = D.`xx'
   }
   local fil = upper("$fil")
   label var `xx'_cyc "Cyclical component of `xx' (`fil')"
}

save ../../data/interim/markups_cyc_$fil.dta, replace


noi disp as text "$S_DATE $S_TIME  end markups_cyc.do"

if "$S_CONSOLE"=="console" & "$batch"!="1" exit, STATA clear
