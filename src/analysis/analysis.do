// analysis.do
//
// Master script for producing paper results
//
// "The Cyclical Behavior of the Price-Cost Markup"
// By Christopher J. Nekarda and Valerie A. Ramey
//
// See instructions in project README.
//
// -------------------------------------------------------------------------- //

noi disp as text "$S_DATE $S_TIME  begin analysis.do"

// project-wide Stata settings
noi run ../_config.do


// run programs as part of master script? (0 = no, 1 = yes)
global batch = 1


// choose standard error for IRF
/* global ci = "nose"  // no SE */
/* global ci = ""  // use asymptotic SE */
global ci = "bs rep(400)"  // use bootstrapped SE


// filter for extracting cyclical components
global fil = "hp"
/* global fil = "bk" */
/* global fil = "fd" */


// extract cyclical components for each filter
global oldfil = "$fil"
foreach fil in hp bk fd {
   global fil = "`fil'"
   noi run markups_cyc.do
}
global fil = "$oldfil"


// main text figures
noi run fig_nu.do
noi run fig_mu_cd.do
noi run fig_mu_ces.do


// estimate unconditional cyclicality and create table
noi run tab_uncond_cyc.do

// estimate conditional cyclicality -- IRFs are saved in ./est
noi run svarmarkup.do

// calculate beta from IRFs
noi run beta_cond.do

// create conditional cyclicality table
noi run tab_cond_cyc.do

// IRF figures
noi run extract_irfs.do
noi run fig_irfs.do


noi disp as text "$S_DATE $S_TIME  end analysis.do"

if "$S_CONSOLE"=="console" exit, STATA clear
