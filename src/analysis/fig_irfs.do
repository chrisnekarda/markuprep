// fig_irfs.do
//
// Figures for CES markups
//
// "The Cyclical Behavior of the Price-Cost Markup"
// By Christopher J. Nekarda and Valerie A. Ramey
//
// See instructions in project README.
//
// -------------------------------------------------------------------------- //

noi disp as text "$S_DATE $S_TIME  begin fig_irfs.do"

use ../../output/oirf.dta, clear

// only plotting these two measures
keep if markup == "lmu_cd" | markup == "lmu_ces_yk_ums"

// scale for humans (multiply by 100)
unab foo: lmu* lrgdp*
foreach xx of local foo {
   replace `xx' = 100 * `xx'
   rename `xx' `xx'_
}

drop line
reshape wide lmu* lrgdp*, i(shock step) j(markup) s

// make the stata shocks start in period 1
expand 2, gen(exp)
drop if exp == 1 & step > 0
replace step = step + 1 if exp == 0
drop if step > 20
drop exp
unab foo: lmu* lrgdp*
foreach xx of local foo {
   qui replace `xx' = 0 if step == 0
}
sort shock step


// merge in SW 2007 sims
merge 1:1 shock step using ../../data/interim/sw2007_irfs_elasticities, nogen


// scale
egen foo1 = max(lrgdp_lmu_cd), by(shock)
egen foo2 = max(lrgdp_lmu_ces_yk_ums), by(shock)
// average the two measures
egen maxy_nr = rowmean(foo1 foo2)
drop foo1 foo2

// max y response for sw
egen maxy_sw = max(y), by(shock)

gen lrgdp_sw = y / maxy_sw * maxy_nr
gen lmu_sw = pmarkup / maxy_sw * maxy_nr

egen ymax = rowmax(lrgdp* lmu*)
sum ymax
local ymax = .2 * ceil(r(max) / .2)
egen ymin = rowmin(lrgdp* lmu*)
sum ymin
local ymin = .2 * floor(r(min) / .2)

gen byte zero = 0

foreach ss in money govt tfp ist {
   noi disp "drawing `ss'"
   local legendpos = "pos(7) bm(4 0 4 0)"
   if "`ss'"=="ist" {
      local legendpos = ""
   }
   #delim ;
   tw
   (rarea lrgdp_p05_lmu_ces_yk_ums lrgdp_p95_lmu_ces_yk_ums step, col("$c8%20") lc(white) lw(vvthin))
   (rarea lrgdp_p05_lmu_cd lrgdp_p95_lmu_cd step, col("$c7%20") lc(white) lw(vvthin))
   (line zero step, yaxis(1 2) lw(thin) lc(gs0))
   (line lrgdp_sw step, lw(medium) lp(shortdash) lc("$c6"))
   (line lrgdp_lmu_cd step, yaxis(1 2) lw(medthick) lc("$c7"))
   (line lrgdp_lmu_ces_yk_ums step, yaxis(1 2) lw(medthick) lc("$c8"))
   if shock=="`ss'",
   yline(0, lc(gs0) lw(thin))
   ylab(`ymin'(.2)`ymax', axis(2) format(%-4.1f)) ymtick(`ymax', axis(2) nolab tlen(4.5))
   ylab(`ymin'(.2)`ymax', nolab) ymtick(`ymax', nolab tlen(4.5))
   xtitle("Quarters")
   xsca(r(-.5 20.5))
   xlabel(0(4)20, for(%3.0f)) xmtick(0(1)20)
   legend(order(5 "Cobb-Douglas" 6 "CES w/ variable utilization" 4 "Smets and Wouters (2007)") pos(7) bm(4 0 4 0))
   title("{bf:Output}", nospan)
   name(output, replace)
   scheme(texpdf) xsize(3.24) ysize(2.7)
   ;
   #delim cr
   graph export ../../output/fig_irf_`ss'_output.pdf, replace

   #delim ;
   tw
   (rarea lmu_p05_lmu_ces_yk_ums lmu_p95_lmu_ces_yk_ums step, col("$c8%20") lc(white) lw(vvthin))
   (rarea lmu_p05_lmu_cd lmu_p95_lmu_cd step, col("$c7%20") lc(white) lw(vvthin))
   (line zero step, yaxis(1 2) lw(thin) lc(gs0))
   (line lmu_sw step, lp(shortdash) lc("$c6"))
   (line lmu_lmu_cd step, yaxis(1 2) lw(medthick) lc("$c7"))
   (line lmu_lmu_ces_yk_ums step, yaxis(1 2) lw(medthick) lc("$c8"))
   if shock=="`ss'",
   yline(0, lc(gs0) lw(thin))
   ylab(`ymin'(.2)`ymax', axis(2) format(%-4.1f)) ymtick(`ymax', axis(2) nolab tlen(4.5))
   ylab(`ymin'(.2)`ymax', nolab) ymtick(`ymax', nolab tlen(4.5))
   xtitle("Quarters")
   xsca(r(-.5 20.5))
   xlabel(0(4)20, for(%3.0f)) xmtick(0(1)20)
   legend(order(5 "Cobb-Douglas" 6 "CES w/ variable utilization" 4 "Smets and Wouters (2007)") `legendpos')
   title("{bf:Markup}", nospan)
   name(markup, replace)
   scheme(texpdf) xsize(3.24) ysize(2.7)
   ;
   #delim cr
   graph export ../../output/fig_irf_`ss'_markup.pdf, replace
}


noi disp as text "$S_DATE $S_TIME  end fig_irfs.do"

if "$S_CONSOLE"=="console" & "$batch"!="1" exit, STATA clear
