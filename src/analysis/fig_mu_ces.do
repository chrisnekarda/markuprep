// fig_mu_ces.do
//
// Figures for CES markups
//
// "The Cyclical Behavior of the Price-Cost Markup"
// By Christopher J. Nekarda and Valerie A. Ramey
//
// See instructions in project README.
//
// -------------------------------------------------------------------------- //

noi disp as text "$S_DATE $S_TIME  begin fig_mu_ces.do"

if "$fil" != "hp" & "$fil" != "bk" & "$fil" != "fd" {
   noi disp as error "Must specify a cyclical filter. See analysis.do"
   exit
}

use ../../data/interim/markups_cyc_$fil.dta, clear


local varlist = "lmu_cd lmu_ces_yl lmu_ces_yl_zsvar lmu_ces_yk lmu_ces_yk_ums lmu_ces_yk_ujf"
foreach xx of local varlist {
   qui replace `xx'_cyc = 100 * `xx'_cyc
}


// ---------------------- //
// detrended markup chart //
// ---------------------- //

#delim ;
tw
(line lmu_cd_cyc qdate, yaxis(1 2) lc("$c7") lw(vthick))
(line lmu_ces_yl_cyc qdate, yaxis(1 2) lc("$c9") lw(medium))
(line lmu_ces_yl_zsvar_cyc qdate, yaxis(1 2) lc("$c13") lw(medium))
(line lmu_ces_yk_cyc qdate, yaxis(1 2) lc("$c10") lw(medium))
(line lmu_ces_yk_ujf_cyc qdate, yaxis(1 2) lc("$c11") lw(medium))
(line lmu_ces_yk_ums_cyc qdate, yaxis(1 2) lc("$c8") lw(medium))
(pcarrowi `=lmu_ces_yl_cyc[56]-.1' `=qdate[56]-17' `=lmu_ces_yl_cyc[56]-.05' `=qdate[56]-1' (9) "{&mu}{sub:L}:  acyclical {it:Z}", col("$c9") lw(medthin) mang(15) mfcol("$c9") mlabcol("$c9") mlabsize(medium))
(pcarrowi `=lmu_ces_yl_zsvar_cyc[45]-1' `=qdate[45]-12' `=lmu_ces_yl_zsvar_cyc[45]-.10' `=qdate[45]-1' (9) "{&mu}{sub:L}:  SVAR {it:Z}", col("$c13") lw(medthin) mang(15) mfcol("$c13") mlabcol("$c13") mlabsize(medium))
(pcarrowi 4.34 `=qdate[11]' `=lmu_ces_yk_cyc[11]+.1' `=qdate[11]' (12) "            {&mu}{sub:K}:  constant {it:u}", col("$c10") lw(medthin) mang(15) mfcol("$c10") mlabcol("$c10") mlabsize(medium))
(pcarrowi 4.34 `=qdate[115]-4' `=lmu_ces_yk_ums_cyc[115]+.05' `=qdate[115]-.5' (12) "{&mu}{sub:K}:  Shapiro {it:u}", col("$c8") lw(medthin) mang(15) mfcol("$c8") mlabcol("$c8") mlabsize(medium))
(pcarrowi 4.34 `=qdate[73]-9' `=lmu_ces_yk_ujf_cyc[73]+.05' `=qdate[73]-.5' (12) "{&mu}{sub:K}:  Fernald {it:u}", col("$c11") lw(medthin) mang(15) mfcol("$c11") mlabcol("$c11") mlabsize(medium))
,
ttitle("")
subtitle("Percent deviation from trend", pos(11))
legend(order(1 "Baseline (C-D)") bm(4 0 0 2) size(medium))
ylab(-6(2)6, for(%-6.0f)) ymtick(6, nolab tlen(4.5))
ylab(-6(2)6, axis(2) for(%-6.0f)) ymtick(6, axis(2) nolab tlen(4.5))
yline(0, lc(gs0) lw(medthin))
xtick(`=tq(1947q1)-4.5'(4)`=tq(2017q4)+4.5')
xlab(`=tq(1950q3)-.5'(20)`=tq(2015q3)-.5', notick for(%tqCCYY))
xline(
`=tq(1948q4)-.5'(.25)`=tq(1949q4)-.5'
`=tq(1953q3)-.5'(.25)`=tq(1954q2)-.5'
`=tq(1957q3)-.5'(.25)`=tq(1958q2)-.5'
`=tq(1960q2)-.5'(.25)`=tq(1961q1)-.5'
`=tq(1969q4)-.5'(.25)`=tq(1970q4)-.5'
`=tq(1973q4)-.5'(.25)`=tq(1975q1)-.5'
`=tq(1980q1)-.5'(.25)`=tq(1980q3)-.5'
`=tq(1981q3)-.5'(.25)`=tq(1982q4)-.5'
`=tq(1990q3)-.5'(.25)`=tq(1991q1)-.5'
`=tq(2001q1)-.5'(.25)`=tq(2001q4)-.5'
`=tq(2007q4)-.5'(.25)`=tq(2009q2)-.5'
, lc("$c16") lw(medium))
scheme(texpdf)
name(ces, replace)
;
#delim cr
graph export ../../output/fig_mu_ces_hp.pdf, replace


noi disp as text "$S_DATE $S_TIME  end fig_mu_ces.do"

if "$S_CONSOLE"=="console" & "$batch"!="1" exit, STATA clear
