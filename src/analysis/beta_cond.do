// beta_cond.do
//
// Calculate conditional cyclicality from IRFs
//
// "The Cyclical Behavior of the Price-Cost Markup"
// By Christopher J. Nekarda and Valerie A. Ramey
//
// See instructions in project README.
//
// -------------------------------------------------------------------------- //

// this file must be executed with -run-; it will not work correctly using -do-.

noi disp as text "$S_DATE $S_TIME  begin beta_cond.do"

set more off
set linesize 120  // so the irf tables don't wrap


local varlist = "lmu_cd lmu_cd_ws lmu_cd_ws1 lmu_cd_ws_oh lmu_ces_yl lmu_ces_yl_zsvar lmu_ces_yk lmu_ces_yk_ums lmu_ces_yk_ujf lmu_ces_oh_yl lmu_ces_oh_yl_zsvar lmu_ces_oh_yk lmu_ces_oh_yk_ums lmu_ces_oh_yk_ujf"

cap erase ../../output/beta_cond.dta
foreach xx of local varlist {

   local irf_file = "../../est/irf_`xx'.irf"

   // MONETARY POLICY SHOCK //
   clear
   tempfile log dat
   cap log close
   log using `log', text replace
   noi irf table coirf, set(`irf_file') irf(moneyvar) level(90) stde impulse(ssr) response(lrgdpc `xx')
   log close
   //
   tempname fhr fhw
   local linenum = 0
   file open `fhr' using `log', read
   file open `fhw' using `dat', write replace
   file read `fhr' line
   while r(eof)==0 {
      local linenum = `linenum' + 1
      if `linenum'>=9 & `linenum'<=29 {
         file write `fhw' `"`macval(line)'"' _n
      }
      file read `fhr' line
   }
   file close `fhr'
   file close `fhw'
   //
   infix step 2-5 double lrgdp 12-19 double lrgdp_p05 24-31 double lrgdp_p95 36-42 double lrgdp_se 48-54 double lmu 60-67 double lmu_p05 72-79 double lmu_p95 84-90 double lmu_se 96-102 using `dat', clear
   drop if step==.
   tsset step
   compress
   aorder
   order step
   gen shock = "money"
   gen markup = "`xx'"
   erase `log'
   erase `dat'
   cap confirm file ../../output/beta_cond.dta
   if _rc==0 {
      append using ../../output/beta_cond.dta
   }
   save ../../output/beta_cond.dta, replace

   // GOVT SPENDING SHOCK //
   clear
   tempfile log dat
   cap log close
   log using `log', text replace
   noi irf table coirf, set(`irf_file') irf(govmilvar) level(90) stde impulse(pdvmil_ngdp) response(lrgdpc `xx')
   log close
   //
   tempname fhr fhw
   local linenum = 0
   file open `fhr' using `log', read
   file open `fhw' using `dat', write replace
   file read `fhr' line
   while r(eof)==0 {
      local linenum = `linenum' + 1
      if `linenum'>=9 & `linenum'<=29 {
         file write `fhw' `"`macval(line)'"' _n
      }
      file read `fhr' line
   }
   file close `fhr'
   file close `fhw'
   //
   infix step 2-5 double lrgdp 12-19 double lrgdp_p05 24-31 double lrgdp_p95 36-42 double lrgdp_se 48-54 double lmu 60-67 double lmu_p05 72-79 double lmu_p95 84-90 double lmu_se 96-102 using `dat', clear
   drop if step==.
   tsset step
   compress
   aorder
   order step
   gen shock = "govmil"
   gen markup = "`xx'"
   erase `log'
   erase `dat'
   cap confirm file ../../output/beta_cond.dta
   if _rc==0 {
      append using ../../output/beta_cond.dta
   }
   save ../../output/beta_cond.dta, replace

   // TFP SHOCK //
   clear
   tempfile log dat
   cap log close
   log using `log', text replace
   noi irf table coirf, set(`irf_file') irf(tfpvar) level(90) stde impulse(lz_jf_util) response(lrgdpc `xx')
   log close
   //
   tempname fhr fhw
   local linenum = 0
   file open `fhr' using `log', read
   file open `fhw' using `dat', write replace
   file read `fhr' line
   while r(eof)==0 {
      local linenum = `linenum' + 1
      if `linenum'>=9 & `linenum'<=29 {
         file write `fhw' `"`macval(line)'"' _n
      }
      file read `fhr' line
   }
   file close `fhr'
   file close `fhw'
   //
   infix step 2-5 double lrgdp 12-19 double lrgdp_p05 24-31 double lrgdp_p95 36-42 double lrgdp_se 48-54 double lmu 60-67 double lmu_p05 72-79 double lmu_p95 84-90 double lmu_se 96-102 using `dat', clear
   drop if step==.
   tsset step
   compress
   aorder
   order step
   gen shock = "tfp"
   gen markup = "`xx'"
   erase `log'
   erase `dat'
   cap confirm file ../../output/beta_cond.dta
   if _rc==0 {
      append using ../../output/beta_cond.dta
   }
   save ../../output/beta_cond.dta, replace

   // IST shock //
   clear
   tempfile log dat
   cap log close
   log using `log', text replace
   noi irf table coirf, set(`irf_file') irf(istvar) level(90) stde impulse(ist) response(lrgdpc `xx')
   log close
   //
   tempname fhr fhw
   local linenum = 0
   file open `fhr' using `log', read
   file open `fhw' using `dat', write replace
   file read `fhr' line
   while r(eof)==0 {
      local linenum = `linenum' + 1
      if `linenum'>=9 & `linenum'<=29 {
         file write `fhw' `"`macval(line)'"' _n
      }
      file read `fhr' line
   }
   file close `fhr'
   file close `fhw'
   //
   infix step 2-5 double lrgdp 12-19 double lrgdp_p05 24-31 double lrgdp_p95 36-42 double lrgdp_se 48-54 double lmu 60-67 double lmu_p05 72-79 double lmu_p95 84-90 double lmu_se 96-102 using `dat', clear
   drop if step==.
   tsset step
   compress
   aorder
   order step
   gen shock = "ist"
   gen markup = "`xx'"
   erase `log'
   erase `dat'
   cap confirm file ../../output/beta_cond.dta
   if _rc==0 {
      append using ../../output/beta_cond.dta
   }
   save ../../output/beta_cond.dta, replace
}

gen byte line = 1 if markup=="lmu_cd"
replace line = 2 if markup=="lmu_cd_ws"
replace line = 3 if markup=="lmu_cd_ws1"
replace line = 4 if markup=="lmu_cd_ws_oh"
replace line = 5 if markup=="lmu_ces_yl"
replace line = 6 if markup=="lmu_ces_yl_zsvar"
replace line = 7 if markup=="lmu_ces_yk"
replace line = 8 if markup=="lmu_ces_yk_ums"
replace line = 9 if markup=="lmu_ces_yk_ujf"
replace line = 10 if markup=="lmu_ces_oh_yl"
replace line = 11 if markup=="lmu_ces_oh_yl_zsvar"
replace line = 12 if markup=="lmu_ces_oh_yk"
replace line = 13 if markup=="lmu_ces_oh_yk_ums"
replace line = 14 if markup=="lmu_ces_oh_yk_ujf"

order line markup shock step
sort line shock step
compress
save ../../output/beta_cond.dta, replace


noi disp as text "$S_DATE $S_TIME  end beta_cond.do"

cap log close

if "$S_CONSOLE"=="console" & "$batch"!="1" exit, STATA clear
