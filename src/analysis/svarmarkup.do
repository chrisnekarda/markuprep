// svarmarkup.do
//
// Results reported in table 2: Conditional cyclicality
//
// "The Cyclical Behavior of the Price-Cost Markup"
// By Christopher J. Nekarda and Valerie A. Ramey
//
// See instructions in project README.
//
// -------------------------------------------------------------------------- //

// draw plots? (1 = yes, 0 = no)
local plot = 0

// save IRF files? (1 = yes, 0 = no)
local save_irf = 1

set more off
est drop _all

// variable for date-time stamp
local d = string(clock("`c(current_date)' `c(current_time)'", "DMY hms"), "%tcCCYYNNDD!_HHMM")

cap log close
log using ../../log/svarmarkup_`d'.log, text replace

noi disp as text "$S_DATE $S_TIME  begin svarmarkup.do"


use ../../data/processed/markupcyc_data.dta, clear


// ---------------------------------------------- //
// ESTIMATE THE STRUCTURAL VECTOR AUTOREGRESSIONS //
// ---------------------------------------------- //

// create variables needed for SVARs
gen t = _n
gen t2 = t^2

//gen lrgdp = ln(rgdp)
gen lrgdpc = ln(rgdp / pop)
gen lrgovc = ln(rgov / pop)
gen lpcom = ln(pzall)
gen lp = ln(pgdp)

// switch sign so positive fed funds shock is expansionary
replace rff = -rff
replace ssr = -ssr
// replace ssr with actual funds rate outside the ZLB period
replace ssr = rff if tin(, 2008q4)
replace ssr = rff if tin(2016q4, )

// IST shocks //
gen lpe = ln(ppcendsv / pequip)
label var lpe "log ratio price of PCE ND+SV to price of equipment investment"

gen infl = 4 * ln(pgdp / L.pgdp)


local varlist = "lmu_cd lmu_cd_ws lmu_cd_ws1 lmu_cd_ws_oh lmu_ces_yl lmu_ces_yl_zsvar lmu_ces_yk lmu_ces_yk_ums lmu_ces_yk_ujf lmu_ces_oh_yl lmu_ces_oh_yl_zsvar lmu_ces_oh_yk lmu_ces_oh_yk_ums lmu_ces_oh_yk_ujf"

foreach xx of local varlist {
   // ---------------------------------------------- //
   // MONETARY POLICY SHOCKS
   // CEE monetary VAR w/ SSR //
   // fed funds is defined as its negative so a positive shock is expansionary

   noi var lrgdpc lp lpcom ssr `xx', lags(1/4) exog(t t2)
   noi irf create moneyvar, step(20) $ci set(../../est/moneyvar, replace)
   noi irf table oirf, level(90) impulse(ssr) response(lrgdpc `xx')

   if `plot' {
      irf graph oirf, impulse(ssr) response(lrgdpc) ustep(20) plot1opt(col(navy)) level(90) ci1opts(lc(white) lw(vvthin) fc(navy%20)) xlab(0(4)20, tlen(*2)) xmtick(0(1)20, tlen(*2)) xtitle("Quarters") yline(0, lc(gs0) lw(thin)) scheme(s1color) ysize(2.5) xsize(3.25) graphr(m(medsmall)) title("{bf:Output}") name(output, replace) byopts(legend(off) note(""))
      irf graph oirf, impulse(ssr) response(`xx') ustep(20) plot1opt(col(navy)) level(90) ci1opts(lc(white) lw(vvthin) fc(navy%20)) xlab(0(4)20, tlen(*2)) xmtick(0(1)20, tlen(*2)) xtitle("Quarters") yline(0, lc(gs0) lw(thin)) scheme(s1color) ysize(2.5) xsize(3.25) graphr(m(medsmall)) title("{bf:Markup}") name(markup, replace) byopts(legend(off) note(""))
      graph combine output markup, row(1) scheme(s1color) xsize(6.5) ysize(2.5) title("{bf:CEE VAR w/ SSR - `xx'}") note("Shaded area is 90-percent confidence interval.")
      graph export ../../output/irf_moneyvar.pdf, replace
   }

   // Extract estimated shocks
   // SSR is ordered second to last so you must condition on current values of
   // other variables (excluding the markup)
   reg ssr L(0/4).lrgdpc L(0/4).lp L(0/4).lpcom L(1/4).ssr L(1/4).`xx' t t2
   predict money_shock_`xx', resid


   // ---------------------------------------------- //
   // GOVERNMENT SPENDING SHOCKS
   // Ramey military news shock //

   noi var pdvmil_ngdp lrgdpc lp tb3 `xx', lags(1/4) exog(t t2)
   noi irf create govmilvar, step(20) $ci set(../../est/govmilvar, replace)
   noi irf table oirf, level(90) impulse(pdvmil_ngdp) response(lrgdpc `xx')

   if `plot' {
      irf graph oirf, impulse(pdvmil_ngdp) response(lrgdpc) ustep(20) plot1opt(col(navy)) level(90) ci1opts(lc(white) lw(vvthin) fc(navy%20)) xlab(0(4)20, tlen(*2)) xmtick(0(1)20, tlen(*2)) xtitle("Quarters") yline(0, lc(gs0) lw(thin)) scheme(s1color) ysize(2.5) xsize(3.25) graphr(m(medsmall)) title("{bf:Output}") name(output, replace) byopts(legend(off) note(""))
      irf graph oirf, impulse(pdvmil_ngdp) response(`xx') ustep(20) plot1opt(col(navy)) level(90) ci1opts(lc(white) lw(vvthin) fc(navy%20)) xlab(0(4)20, tlen(*2)) xmtick(0(1)20, tlen(*2)) xtitle("Quarters") yline(0, lc(gs0) lw(thin)) scheme(s1color) ysize(2.5) xsize(3.25) graphr(m(medsmall)) title("{bf:Markup}") name(markup, replace) byopts(legend(off) note(""))
      graph combine output markup, row(1) scheme(s1color) xsize(6.5) ysize(2.5) title("{bf:Ramey military news shock - `xx'}") note("Shaded area is 90-percent confidence interval.")
      graph export ../../output/irf_govmilvar.pdf, replace
   }

   // Extract estimated shock
   // pdvmil_ngdp is ordered first, so do *not* condition on current values of
   // other variables
   reg pdvmil_ngdp L(1/4).pdvmil_ngdp L(1/4).lrgdpc L(1/4).lp L(1/4).tb3 L(1/4).`xx' t t2
   predict govmil_shock_`xx', resid


   // ---------------------------------------------- //
   // TECHNOLOGY SHOCKS
   // Fernald utilization TFP //

   noi var lz_jf_util lrgdpc lp tb3 `xx', lags(1/4) exog(t t2)
   noi irf create tfpvar, step(20) $ci set(../../est/tfpvar, replace)
   noi irf table oirf, level(90) impulse(lz_jf_util) response(lrgdpc `xx')

   if `plot' {
      irf graph oirf, impulse(lz_jf_util) response(lrgdpc) ustep(20) plot1opt(col(navy)) level(90) ci1opts(lc(white) lw(vvthin) fc(navy%20)) xlab(0(4)20, tlen(*2)) xmtick(0(1)20, tlen(*2)) xtitle("Quarters") yline(0, lc(gs0) lw(thin)) scheme(s1color) ysize(2.5) xsize(3.25) graphr(m(medsmall)) title("{bf:Output}") name(output, replace) byopts(legend(off) note(""))
      irf graph oirf, impulse(lz_jf_util) response(`xx') ustep(20) plot1opt(col(navy)) level(90) ci1opts(lc(white) lw(vvthin) fc(navy%20)) xlab(0(4)20, tlen(*2)) xmtick(0(1)20, tlen(*2)) xtitle("Quarters") yline(0, lc(gs0) lw(thin)) scheme(s1color) ysize(2.5) xsize(3.25) graphr(m(medsmall)) title("{bf:Markup}") name(markup, replace) byopts(legend(off) note(""))
      graph combine output markup, row(1) scheme(s1color) xsize(6.5) ysize(2.5) title("{bf:Tech shock - `xx'}") note("Shaded area is 90-percent confidence interval.")
      graph export ../../output/irf_tfpvar.pdf, replace
   }

   // Extract estimated shock
   // lz_jf_util is ordered first, so do *not* condition on current values of
   // other variables
   reg lz_jf_util L(1/4).lz_jf_util L(1/4).lrgdpc L(1/4).lp L(1/4).tb3 L(1/4).`xx' t t2
   predict tfp_shock_`xx', resid


   // IST shocks //
   noi ivreg2 D.lpe (D2.lrgdpc D.infl D.tb3 D.`xx' = L4.D.lrgdpc L4.infl L4.tb3 L4.`xx') L(1/3).D2.lrgdpc L(1/3).D.infl L(1/3).D.tb3 L(1/3).D.`xx' L(1/4).D.lpe t t2
   predict ist, resid

   // for some strange reason, can't do bootstrapped SE for IST regressions
   // but only for some variables
    if substr("$ci", 1, 2)=="bs" {
      noi var ist lpe lrgdpc lp tb3 `xx', lags(1/4) exog(t t2)
      noi irf create istvar, step(20) set(../../est/istvar, replace)
      noi irf table oirf, level(90) impulse(ist) response(lrgdpc `xx')
   }
   else {
      noi var ist lpe lrgdpc lp tb3 `xx', lags(1/4) exog(t t2)
      noi irf create istvar, step(20) $ci set(../../est/istvar, replace)
      noi irf table oirf, level(90) impulse(ist) response(lrgdpc `xx')
   }
   if `plot' {
      irf graph oirf, impulse(ist) response(lrgdpc) ustep(20) plot1opt(col(navy)) level(90) ci1opts(lc(white) lw(vvthin) fc(navy%20)) xlab(0(4)20, tlen(*2)) xmtick(0(1)20, tlen(*2)) xtitle("Quarters") yline(0, lc(gs0) lw(thin)) scheme(s1color) ysize(2.5) xsize(3.25) graphr(m(medsmall)) title("{bf:Output}") name(output, replace) byopts(legend(off) note(""))
      irf graph oirf, impulse(ist) response(`xx') ustep(20) plot1opt(col(navy)) level(90) ci1opts(lc(white) lw(vvthin) fc(navy%20)) xlab(0(4)20, tlen(*2)) xmtick(0(1)20, tlen(*2)) xtitle("Quarters") yline(0, lc(gs0) lw(thin)) scheme(s1color) ysize(2.5) xsize(3.25) graphr(m(medsmall)) title("{bf:Markup}") name(markup, replace) byopts(legend(off) note(""))
      graph combine output markup, row(1) scheme(s1color) xsize(6.5) ysize(2.5) title("{bf:IST shock - `xx'}") note("Shaded area is 90-percent confidence interval.")
      graph export ../../output/irf_istvar.pdf, replace
   }
   drop ist

   // Extract estimated shock
   // Long-run restrictions imposed using Shapiro-Watson (1988) IV method
   noi ivreg2 D.lpe (D2.lrgdpc D.infl D.tb3 D.`xx' = L4.D.lrgdpc L4.infl L4.tb3 L4.`xx') L(1/3).D2.lrgdpc L(1/3).D.infl L(1/3).D.tb3 L(1/3).D.`xx' L(1/4).D.lpe t t2
   predict ist_shock_`xx', resid


   // ---------------------------------------------- //
   if `plot' & "$S_OS" != "Windows" {
      !gs -q -dSAFER -dBATCH -dNOPAUSE -sOutputFile=irf_`xx'.pdf -sDEVICE=pdfwrite -f irf_moneyvar.pdf irf_govmilvar.pdf irf_tfpvar.pdf irf_istvar.pdf
   }
   cap erase irf_moneyvar.pdf
   cap erase irf_govmilvar.pdf
   cap erase irf_tfpvar.pdf
   cap erase irf_istvar.pdf

   // ---------------------------------------------- //
   // compile all results into single IRF set
   if `save_irf' {
      irf set ../../est/irf_`xx', replace
      irf add _all, using(../../est/moneyvar)
      irf add _all, using(../../est/govmilvar)
      irf add _all, using(../../est/tfpvar)
      irf add _all, using(../../est/istvar)
   }
   cap erase ../../est/moneyvar.irf
   cap erase ../../est/govmilvar.irf
   cap erase ../../est/tfpvar.irf
   cap erase ../../est/istvar.irf
}

save ../../data/interim/svarmarkup.dta, replace

preserve
keep qdate *shock*

reshape long money_shock_ govmil_shock_ tfp_shock_ ist_shock_, i(qdate) j(markup) string

label var markup "Markup measure"

rename money_shock_ money_shock
label var money_shock "Estimated monetary policy shock"

rename govmil_shock_ govmil_shock
label var govmil_shock "Estimated government spending shock"

rename tfp_shock_ tfp_shock
label var tfp_shock "Estimated TFP shock"

rename ist_shock_ ist_shock
label var ist_shock "Estimated invest. specific technology shock"

compress
sort markup qdate
label data "Estimated shocks from 'The Cyclical Behavior of the Price-Cost Markup'"
save ../../output/estimated_shocks.dta, replace

noi des, f
restore


noi disp as text "$S_DATE $S_TIME  end svarmarkup.do"

cap log close

if "$S_CONSOLE"=="console" & "$batch"!="1" exit, STATA clear
