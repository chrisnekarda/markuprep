// fig_nu.do
//
// Share of production workers in total private employment
//
// "The Cyclical Behavior of the Price-Cost Markup"
// By Christopher J. Nekarda and Valerie A. Ramey
//
// See instructions in project README.
//
// -------------------------------------------------------------------------- //

noi disp as text "$S_DATE $S_TIME  begin fig_nu.do"

use ../../data/processed/markupcyc_data.dta, clear


// ----------------------- //
// share of variable labor //
// ----------------------- //

qui replace nu = 100 * nu


#delim ;
tw
(line nu qdate, yaxis(1 2) lc("$c7"))
,
ttitle("")
subtitle("Percent", pos(11))
legend(off)
ylab(79(1)85, for(%-6.0f)) ymtick(85, nolab tlen(4.5))
ylab(79(1)85, axis(2) for(%-6.0f)) ymtick(85, axis(2) nolab tlen(4.5))
ytitle("") ytitle("", axis(2))
xtick(`=tq(1947q1)-4.5'(4)`=tq(2017q4)+4.5')
xlab(`=tq(1950q3)-.5'(20)`=tq(2015q3)-.5', notick for(%tqCCYY))
xline(
`=tq(1948q4)-.5'(.25)`=tq(1949q4)-.5'
`=tq(1953q3)-.5'(.25)`=tq(1954q2)-.5'
`=tq(1957q3)-.5'(.25)`=tq(1958q2)-.5'
`=tq(1960q2)-.5'(.25)`=tq(1961q1)-.5'
`=tq(1969q4)-.5'(.25)`=tq(1970q4)-.5'
`=tq(1973q4)-.5'(.25)`=tq(1975q1)-.5'
`=tq(1980q1)-.5'(.25)`=tq(1980q3)-.5'
`=tq(1981q3)-.5'(.25)`=tq(1982q4)-.5'
`=tq(1990q3)-.5'(.25)`=tq(1991q1)-.5'
`=tq(2001q1)-.5'(.25)`=tq(2001q4)-.5'
`=tq(2007q4)-.5'(.25)`=tq(2009q2)-.5'
, lc("$c16") lw(medium))
scheme(texpdf)
name(nu, replace)
;
#delim cr

graph export ../../output/fig_nu.pdf, replace

noi disp as text "$S_DATE $S_TIME  end fig_nu.do"

if "$S_CONSOLE"=="console" & "$batch"!="1" exit, STATA clear
