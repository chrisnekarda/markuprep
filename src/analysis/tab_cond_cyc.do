// tab_cond_cyc.do
//
// Results reported in table 2: Conditional cyclicality
//
// "The Cyclical Behavior of the Price-Cost Markup"
// By Christopher J. Nekarda and Valerie A. Ramey
//
// See instructions in project README.

// -------------------------------------------------------------------------- //

// variable for date-time stamp
local d = string(clock("`c(current_date)' `c(current_time)'", "DMY hms"), "%tcCCYYNNDD!_HHMM")

cap log close
log using ../../log/tab_cond_cyc_`d'.log, text replace

noi disp as text "$S_DATE $S_TIME  begin tab_cond_cyc.do"

use ../../output/beta_cond.dta, clear

// rescale for humans (multiply by 100)
unab foo: lmu* lrgdp*
foreach xx of local foo {
   replace `xx' = 100 * `xx'
}

gen beta = lmu / lrgdp

replace shock = "1_money" if shock=="money"
replace shock = "2_govt" if shock=="govmil"
replace shock = "3_tfp" if shock=="tfp"
replace shock = "4_ist" if shock=="ist"

// note: step 19 is 20 quarters
noi table line shock if step==19, c(mean beta) for(%5.2f)


noi disp as text "$S_DATE $S_TIME  end tab_cond_cyc.do"

cap log close

if "$S_CONSOLE"=="console" & "$batch"!="1" exit, STATA clear
