// fig_mu_cd.do
//
// Figures for Cobb-Douglas markups
//
// "The Cyclical Behavior of the Price-Cost Markup"
// By Christopher J. Nekarda and Valerie A. Ramey
//
// See instructions in project README.
//
// -------------------------------------------------------------------------- //

noi disp as text "$S_DATE $S_TIME  begin fig_mu_cd.do"

if "$fil" != "hp" & "$fil" != "bk" & "$fil" != "fd" {
   noi disp as error "Must specify a cyclical filter. See analysis.do"
   exit
}

use ../../data/interim/markups_cyc_$fil.dta, clear


// --------------------- //
// markup chart - levels //
// --------------------- //

// create index variable
sum mu_bus if tin(2012q1, 2012q4)
gen i_mu_bus = mu_bus / r(mean) * 100

#delim ;
tw
(line i_mu_bus qdate, yaxis(1 2) lc("$c7"))
,
ttitle("")
subtitle("Index, 2012 = 100", pos(11))
legend(off)
ylab(80(5)105, for(%-6.0f)) ymtick(105, nolab tlen(4.5))
ylab(80(5)105, axis(2) for(%-6.0f)) ymtick(105, axis(2) nolab tlen(4.5))
ytitle("") ytitle("", axis(2))
xtick(`=tq(1947q1)-4.5'(4)`=tq(2017q4)+4.5')
xlab(`=tq(1950q3)-.5'(20)`=tq(2015q3)-.5', notick for(%tqCCYY))
xline(
`=tq(1948q4)-.5'(.25)`=tq(1949q4)-.5'
`=tq(1953q3)-.5'(.25)`=tq(1954q2)-.5'
`=tq(1957q3)-.5'(.25)`=tq(1958q2)-.5'
`=tq(1960q2)-.5'(.25)`=tq(1961q1)-.5'
`=tq(1969q4)-.5'(.25)`=tq(1970q4)-.5'
`=tq(1973q4)-.5'(.25)`=tq(1975q1)-.5'
`=tq(1980q1)-.5'(.25)`=tq(1980q3)-.5'
`=tq(1981q3)-.5'(.25)`=tq(1982q4)-.5'
`=tq(1990q3)-.5'(.25)`=tq(1991q1)-.5'
`=tq(2001q1)-.5'(.25)`=tq(2001q4)-.5'
`=tq(2007q4)-.5'(.25)`=tq(2009q2)-.5'
, lc("$c16") lw(medium))
scheme(texpdf)
name(mu_cd_lev, replace)
;
#delim cr

graph export ../../output/fig_mu_cd_lev.pdf, replace


// ---------------------- //
// detrended markup chart //
// ---------------------- //
foreach xx in lmu_cd lmu_cd_ws lmu_cd_ws_oh {
   qui replace `xx'_cyc = 100 * `xx'_cyc
}


#delim ;
tw
(line lmu_cd_ws_oh_cyc qdate, yaxis(1 2) lc("$c9"))
(line lmu_cd_ws_cyc qdate, yaxis(1 2) lc("$c8"))
(line lmu_cd_cyc qdate, yaxis(1 2) lc("$c7"))
,
ttitle("")
subtitle("Percent deviation from trend", pos(11))
legend(order(3 "Labor compensation" 2 "Wages and salaries" 1 "Variable wages and salaries") col(2) colfirst bm(4 0 0 2))
ylab(-4(1)4, for(%-4.0f)) ymtick(4, nolab tlen(4.5))
ylab(-4(1)4, axis(2) for(%-4.0f)) ymtick(4, axis(2) nolab tlen(4.5))
yline(0, lc(gs0) lw(medthin))
xtick(`=tq(1947q1)-4.5'(4)`=tq(2017q4)+4.5')
xlab(`=tq(1950q3)-.5'(20)`=tq(2015q3)-.5', notick for(%tqCCYY))
xline(
`=tq(1948q4)-.5'(.25)`=tq(1949q4)-.5'
`=tq(1953q3)-.5'(.25)`=tq(1954q2)-.5'
`=tq(1957q3)-.5'(.25)`=tq(1958q2)-.5'
`=tq(1960q2)-.5'(.25)`=tq(1961q1)-.5'
`=tq(1969q4)-.5'(.25)`=tq(1970q4)-.5'
`=tq(1973q4)-.5'(.25)`=tq(1975q1)-.5'
`=tq(1980q1)-.5'(.25)`=tq(1980q3)-.5'
`=tq(1981q3)-.5'(.25)`=tq(1982q4)-.5'
`=tq(1990q3)-.5'(.25)`=tq(1991q1)-.5'
`=tq(2001q1)-.5'(.25)`=tq(2001q4)-.5'
`=tq(2007q4)-.5'(.25)`=tq(2009q2)-.5'
, lc("$c16") lw(medium))
scheme(texpdf)
name(mu_cd_hp, replace)
;
#delim cr

graph export ../../output/fig_mu_cd_hp.pdf, replace


noi disp as text "$S_DATE $S_TIME  end fig_mu_cd.do"

if "$S_CONSOLE"=="console" & "$batch"!="1" exit, STATA clear
