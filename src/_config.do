// _config.do
//
// Project-wide Stata settings
//
// "The Cyclical Behavior of the Price-Cost Markup"
// By Christopher J. Nekarda and Valerie A. Ramey
//
// See instructions in project README.
//
// -------------------------------------------------------------------------- //

clear all
set type double
set more off
set matsize 2000


// remove any personal directories from the adopath //
while _rc == 0 {
   cap adopath -4
}


// add path to our external dependencies //

// get current working directory
local cwd = c(pwd)
local len = length("`cwd'")
// strip trailing "analysis" or "appendix"
if substr("`cwd'", -8, .) == "analysis" | ///
   substr("`cwd'", -8, .) == "appendix" {
   local root_path = substr("`cwd'", 1, `=`len'-9')
}
// strip trailing "data"
else if substr("`cwd'", -4, .) == "data" {
   local root_path = substr("`cwd'", 1, `=`len'-5')
}
else if substr("`cwd'", -3, .) == "src" {
   // if run from ./src
   local root_path = "`cwd'"
}
else {
   noi disp as error "_config.do must be run from a directory in ./src"
   error 99
}

adopath + "`root_path'"
adopath + "`root_path'/external"

// force stata to search only the project directory for "personal" ado files
sysdir set PERSONAL "`root_path'/external"

// update MATA libraries index
mata: mata mlib index

// graphics scheme for figures
sysdir set PLUS "`root_path'"
discard
set scheme texpdf

// define custom colors (MATLAB Parula scheme) //

// (gray)
global c6  = " 77  77  77"
// (blue)
global c7  = "  0 114 189"
// (orange)
global c8  = "217  83  25"
// (yellow)
global c9  = "237 177  32"
// (purple)
global c10 = "126  47 142"
// (green)
global c11 = "119 172  48"
// (light blue)
global c12 = " 77 190 238"
// (red)
global c13 = "162  20  47"
// (dark green)
global c14 = " 78 112  31"
// forecast shading
global c15 = "224 255 255"
// recession shading
global c16 = "217 217 217"
