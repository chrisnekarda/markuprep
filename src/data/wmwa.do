// wmwa.do
//
// Construct marginal-average wage factor for marginal markup measures
//
// "The Cyclical Behavior of the Price-Cost Markup"
// By Christopher J. Nekarda and Valerie A. Ramey
//
// See instructions in project README. Global environment settings for
// data construction are in build_data.do.
// -------------------------------------------------------------------------- //

noi disp as text "$S_DATE $S_TIME  begin wmwa.do"


use ../../data/raw/dvdh_cps_pri.dta, clear
label data ""

// collapse monthly data to quarterly
tscollap h v dvdh, to(q) gen(qdate)
tsset qdate, q
note qdate: Quarter, 1960:Q1 = 1
order qdate

drop if h == .

label var h "Average weekly hours per person"
label var v "Average weekly overtime hours (h>40) per person"
label var dvdh "Change in overtime hours over change in average hours"

// ratio of overtime hours to average hours
gen vh = v/h
label var vh "Ratio of overtime hours to average hours"

// fraction of 40+ hours/wk covered by overtime premium
gen theta = 0.3
label var theta "Fraction of hours 40+ hours/wk covered by overtime premium"

// marginal-average factor
gen wmwa_50 = (1 + .50 * theta * dvdh) / (1 + .50 * theta * vh)
label var wmwa_50 "Marginal-average factor, 50% premium for overtime hours"


// SAVE AND EXIT
compress
note: Version $ver, created $S_DATE $S_TIME
save ../../data/interim/wmwa.dta, replace

noi des, f

noi disp as text "$S_DATE $S_TIME  end wmwa.do"

if "$S_CONSOLE"=="console" & "$batch"!="1" exit, STATA clear
