//  kstock.do
//
// Create quarterly estimate of productive capital stock
//
// "The Cyclical Behavior of the Price-Cost Markup"
// By Christopher J. Nekarda and Valerie A. Ramey
//
// See instructions in project README. Global environment settings for
// data construction are in build_data.do.
// -------------------------------------------------------------------------- //

noi disp as text "$S_DATE $S_TIME  begin kstock.do"


// -------------------- //
// FIXED ASSET ACCOUNTS //
// -------------------- //

// source
// Table 1.2. Chain-Type Quantity Indexes for Net Stock of Fixed Assets and Consumer Durable Goods
// https://apps.bea.gov/iTable/iTable.cfm?reqid=10&step=3&isuri=1&table_list=17&series=q&first_year=2010&allyears=1&tabledisplay=&scale=-99&last_year=2017

import excel using ../../data/raw/Capital_Stock_and_Investment_data.xlsx, first sheet("annual_k") clear
tsset year, y

sum realk_privfixed if year==1946
scalar qrk_1946 = r(mean)

sum realk_privbus if year==2012
scalar rk_2012 = r(mean)

// calculate first-difference for flows interpolation
foreach var in realk_all realk_fixed realk_privfixed {
  gen d_`var' = D.`var'
}

keep if tin(1947, 2017)
compress
save ../../data/interim/fixed_assets.dta, replace


// INTERPOLATE ANNUAL CAPITAL STOCK TO QUARTERLY //

// interpolate to quarterly using Denton method
tempfile denton
denton d_realk_privfixed using `denton', interp(rinvest_priv_fixed) from(../../data/interim/fred_quarterly.dta) gen(d_realk_privfixed_interp)
use `denton', clear
order qdate

label var d_realk_privfixed "Change in net stock of fixed assets, annual"
note d_realk_privfixed: d_realk_privfixed = D.realk_privfixed

// keep the quarterly change at an annual rate
replace d_realk_privfixed_interp = d_realk_privfixed_interp * 4
label var d_realk_privfixed_interp "Change in net stock of fixed assets, annual rate"
note d_realk_privfixed_interp: Denton interpolation of d_realk_privfixed using rinvest_priv_fixed as indicator; multiplied by 4

// net stock of fixed assets, quarterly (quantity index)
gen qk_privfixed = qrk_1946 + sum(d_realk_privfixed_interp/4)
label var qk_privfixed "Quantity indexes for net stock of fixed assets, private sector"
note qk_privfixed: qk_privfixed = [quantity index for fixed assets in 1946] + sum(d_realk_privfixed_interp/4)

// real capital stock, private business
gen rk_privbus = qk_privfixed / 100 * rk_2012
label var rk_privbus "Real productive capital stock, private business (chained 2012 dollars)"
note rk_privbus: rk_privbus = qk_privfixed / 100 * [annual productive capital stock in 2012 (billions)]

// clean up and save
compress
note: Version $ver, created $S_DATE $S_TIME
save ../../data/interim/kstock.dta, replace

noi des, f

noi disp as text "$S_DATE $S_TIME  end kstock.do"

if "$S_CONSOLE"=="console" & "$batch"!="1" exit, STATA clear
