// pcendsv.do
//
// Construct consumption of nondurables plus services
//
// "The Cyclical Behavior of the Price-Cost Markup"
// By Christopher J. Nekarda and Valerie A. Ramey
//
// See instructions in project README. Global environment settings for
// data construction are in build_data.do.
// -------------------------------------------------------------------------- //

noi disp as text "$S_DATE $S_TIME  begin pcendsv.do"


// REAL PCE NONDURABLES PLUS SERVICES //
// This worksheet uses Wheelan's method to construct consumption of nondurables plus services
import excel ../../data/raw/Relative_Equipment_Price_Data.xlsx, sheet("Chain") cellrange(A3) firstrow case(lower) clear

// create date variable
rename year quarter
gen year = floor(quarter)
gen qtr = 4 * (quarter-year) + 1
gen int qdate=yq(year, qtr)
drop if qdate == .
tsset qdate, q

keep qdate rcndsv
rename rcndsv rpcendsv

label var rpcendsv "Real consumption of nondurables plus services"
note rpcendsv: Source: Authors' calculations from BEA data


// SAVE TEMP FILE
save ../../data/interim/pcendsv.dta, replace



// PRICE INDEX PCE NONDURABLES PLUS SERVICES //

// This worksheet uses Wheelan's method to construct consumption of nondurables plus services
import excel ../../data/raw/Relative_Equipment_Price_Data.xlsx, first sheet("main") clear

gen year = floor(quarter)
gen qtr = 4 * (quarter - year) + 1
gen int qdate = yq(year, qtr)
note qdate: Quarter, 1960:Q1 = 1

tsset qdate, q
order qdate

// 2020-02-24: all other variables are now downloaded directly from FRED
keep qdate pcndsv
rename pcndsv ppcendsv

label var ppcendsv "Price index, PCE nondurable goods plus services"
note ppcendsv: Source: Authors' calculations from BEA data

merge 1:1 qdate using ../../data/interim/pcendsv.dta, nogen
sort qdate

compress
note: Version $ver, created $S_DATE $S_TIME
save ../../data/interim/pcendsv.dta, replace

noi des, f

noi disp as text "$S_DATE $S_TIME  end pcendsv.do"

if "$S_CONSOLE"=="console" & "$batch"!="1" exit, STATA clear
