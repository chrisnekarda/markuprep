// bivartech.do
//
// Estimates aggregate technology and non-technology shocks and technology
// level using Gali (1999) long-run restrictions
//
// "The Cyclical Behavior of the Price-Cost Markup"
// By Christopher J. Nekarda and Valerie A. Ramey
//
// See instructions in project README. Global environment settings for
// data construction are in build_data.do.
// -------------------------------------------------------------------------- //

//log using svar.log, text replace
//set linesize 120

noi disp as text "$S_DATE $S_TIME  begin bivartech.do"

// GET REQUIRED SERIES //
use ../../data/interim/lpc_data.dta, clear
merge 1:1 qdate using  ../../data/interim/fred_quarterly.dta, keepusing(pop) nogen
sort qdate


// DEFINE VARIABLES //

// real output per hour of all persons, business sector
gen lx = ln(output_bus/(pd_bus/100)) - ln(hours_bus)
label var lx "Real output per hour of all persons, business sector"

// per capita hours in business
gen ln = ln(hours_bus/pop)
label var ln "Business sector hours per capita"


// -----------------------------------------------------------------------------
//   IMPORTANT: CHOOSE WHETHER HOURS are IN LEVELS OR FIRST DIFFERENCES
// -----------------------------------------------------------------------------

local spec = "fd"  // "lev" for levels of hours, "fd" for first difference of hours
local irf = 0  // 1 = draw IRF

// ------------------------------------- //
// CREATE VARIABLES FOR SVAR REGRESSIONS //
// ------------------------------------- //

gen t = _n
gen t2 = t^2
gen t3 = t^3

gen constantvar = 1  // necessary for IRFs when we want to normalize to 0

gen d_lx = D.lx
gen d_ln = D.ln


// -------------------------------------------------- //
// DEFINE WHETHER HOURS ARE LOG LEVELS OR DIFFERENCES //
// If log levels, include a polynomial trend          //
// -------------------------------------------------- //

if "`spec'"=="lev" {
   gen hoursvar = ln
   global trendlist constantvar t t2 t3
}
else {
   gen hoursvar = d_ln
   global trendlist constantvar
}

gen dhoursvar = D.hoursvar

// quick check definition of hours variable
summ hoursvar hoursvar ln d_ln


// ----------------------------------------------------------------- //
// ESTIMATE LR RESTRICTION MODEL USING SHAPIRO-WATSON 1988 IV METHOD //
// ----------------------------------------------------------------- //

ivregress 2sls d_lx L(1/4).d_lx (dhoursvar  = L4.hoursvar) L(1/3).dhoursvar $trendlist, nocons

predict d_lz_tech_`spec', resid  /* this is the identified technology shock */
estimates store d_lxiv


reg hoursvar L(1/4).hoursvar L(1/4).d_lx d_lz_tech_`spec' $trendlist, nocons

predict d_lx_nontech_`spec', resid /* this is the identified non-technology shock */
estimate store hoursreg


// CREATE SIMULATION MODEL FROM THE REGRESSIONS
forecast create lrmodel, replace
forecast estimates d_lxiv
forecast estimates hoursreg

forecast identity lx = L.lx + d_lx
forecast identity dhoursvar = D.hoursvar

if "`irf'"=="1" {
   // ----------------------------------------------------------------- //
   // COMMANDS FOR CREATING IRFS
   // ----------------------------------------------------------------- //
   preserve

   // Initialize to 0 for IRF simulation
   foreach xx in constantvar lx d_lx hoursvar dhoursvar d_lz_tech_`spec' d_lx_nontech_`spec' {
      replace `xx' = 0
   }

   forecast adjust d_lx = d_lx + 1 if qdate==tq(2001q1)

   replace d_lz_tech_`spec' = 1 if qdate==tq(2001q1)

   // Can change end point to use longer or shorter horizons for IRFs

   forecast solve, begin(tq(2001q1)) end(tq(2005q4))

   gen irf_lx = f_lx

   if "`spec'"=="lev" {
      gen irf_ln = f_hoursvar
   }
   else {
      gen irf_ln = sum(f_hoursvar)
   }

   gen byte x = qdate-tq(2001q1)

   tw (line irf_lx x, yaxis(1 2)) if x>=0 & x<20, ttitle("Quarters") tlab(0(4)20) tmtick(0(1)19) legend(off) yline(0, lc(gs0) lw(thin)) /*ylab(0(.2)1) ylab(0(.2)1, axis(2))*/ ytitle("") ytitle("", axis(2)) scheme(s1color) title("{bf:Productivity}") name(lx, replace)

   tw (line irf_ln x, yaxis(1 2)) if x>=0 & x<20, ttitle("Quarters") tlab(0(4)20) tmtick(0(1)19) legend(off) yline(0, lc(gs0) lw(thin)) /*ylab(-1(.2)0) ylab(-1(.2)0, axis(2))*/ ytitle("") ytitle("", axis(2)) scheme(s1color) title("{bf:Hours}") name(ln, replace)

   graph combine lx ln, row(1) xsize(6.5) ysize(3) name(irf, replace)
   graph drop lx ln

   list x irf_lx irf_ln if x>=0 & x<20

   restore
}
else{
   // -------------------------------- //
   // SIMULATE THE LEVEL OF TECHNOLOGY //
   // -------------------------------- //

   // Augment d_lx with tech shock.
   forecast adjust d_lx = d_lx + d_lz_tech_`spec' if d_lz_tech_`spec'!=.

   forecast solve, begin(tq(1949q2)) end(tq(2017q4))

   // Estimated log level of technology
   rename f_lx lz_tech_`spec'

   if "`spec'"=="lev" {
      label var lz_tech_`spec' "Estimated log technology level, LR restrictions, hours in levels"
   }
   else {
      label var lz_tech_`spec' "Estimated log technology level, LR restrictions, hours in differences"
   }

   keep qdate lx lz_tech_`spec'

   compress
   note: Version $ver, created $S_DATE $S_TIME
   save ../../data/interim/lz_tech_`spec'.dta, replace

   noi des, f

   if "$batch"!="1" {
      tw (line lx qdate, yaxis(1 2)) (line lz_tech_`spec' qdate, yaxis(1 2)), ttitle("") scheme(s1color) title("{bf:Productivity}") name(lx, replace)
      noi list qdate lx f_lx
   }
}

noi disp as text "$S_DATE $S_TIME  end bivartech.do"

if "$S_CONSOLE"=="console" & "$batch"!="1" exit, STATA clear
