// build_data.do
//
// Master script for constructing data files
//
// "The Cyclical Behavior of the Price-Cost Markup"
// By Christopher J. Nekarda and Valerie A. Ramey
//
// See instructions in project README. Run download_data.do to update source
// data.
//
// -------------------------------------------------------------------------- //

noi disp as text "$S_DATE $S_TIME  begin build_data.do"

// project-wide Stata settings
noi run ../_config.do


// run programs as part of master script? (0 = no, 1 = yes)
global batch = 1


// data set version
global ver = 6.2


// Fernald (2014) TFP and utilization measures
noi run quarterly_tfp.do

// Process DLEU markup and interpolate annual to quarterly for charts
noi run dleu_markup.do

// Load Smets-Wouters (2007) IRFs and elasticities
noi run sw2007_irfs_elasticities.do

// Krippner (2013) shadow short rate (SSR)
noi run krippner_ssr.do

// Output, hours, compensation for BUS and NFCB
noi run lpc_data.do

// Minor transformations of raw data from FRED
noi run transform_data.do

// Calculate wages and salaries for variable labor, private business
noi run ws_bus_vari.do

// Consumption of nondurables plus services
noi run pcendsv.do

// quarterly estimate of productive capital stock
noi run kstock.do

// technology level and technology & nontechnology shocks
noi run bivartech.do

// Construct marginal-average wage factor for marginal markup measures
noi run wmwa.do

// Assemble data apart from markups (constructed later)
noi run markupcyc_data.do

// Create markup measures for analysis
noi run create_markups.do


// load the full data set (saved in create_markups.do)
use ../../data/processed/markupcyc_data.dta, replace

noi tsset

noi des, f


// export data sets //

// as XLSX
export excel using ../../data/processed/markupcyc_data.xlsx, replace firstrow(var)

// as csv
preserve
gen Date = dofm(mofd(dofq(qdate)) + 3) - 1
format Date %tdCCYYNNDD
order Date
drop qdate
outsheet using ../../data/processed/markupcyc_data.csv, comma replace
restore

noi disp as text "$S_DATE $S_TIME  end build_data.do"

if "$S_CONSOLE" == "console" exit, STATA clear
