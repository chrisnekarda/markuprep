// lpc_data.do
//
// Read in LPC special request files (not available in FRED)
//
// "The Cyclical Behavior of the Price-Cost Markup"
// By Christopher J. Nekarda and Valerie A. Ramey
//
// See instructions in project README. Global environment settings for
// data construction are in build_data.do.
//
// lpc_data come from BLS special request files:
// https://www.bls.gov/lpc/special_requests/nonfarm_business.zip
// https://www.bls.gov/lpc/special_requests/nonfinancial_corporate.zip
// see download_data.do
// -------------------------------------------------------------------------- //

noi disp as text "$S_DATE $S_TIME  begin lpc_data.do"


// PRIVATE BUSINESS SECTOR //
tempfile level index

// GET FILE DATE for souce note
insheet using ../../data/raw/nonfarm_business-quarterly-series.date, clear
local filedate = v1[1]


// series in indexes
import excel ../../data/raw/nonfarm_business-quarterly-series.xlsx, clear sheet("BUS, All persons (Index)") cellrange(A7) firstrow case(lower)
gen int qdate = yq(year, qtr)

// remove rows with stray headers
keep if qdate != .

keep qdate implicitpricedeflator

// de-string
destring implicitpricedeflator, replace force

rename implicitpricedeflator pd_bus

label var pd_bus "Implicit price deflator, private business"

save `index'

// series in levels
import excel ../../data/raw/nonfarm_business-quarterly-series.xlsx, clear sheet("BUS, All persons (Level)") cellrange(A7) firstrow case(lower)
gen int qdate = yq(year, qtr)
tsset qdate, q
note qdate: Quarter, 1960:Q1 = 1
order qdate

// remove rows with stray headers
keep if qdate != .

keep qdate hoursworked hourlycompensation employment compensation currentdollaroutput

// de-string
foreach xx in hoursworked hourlycompensation employment compensation currentdollaroutput {
   destring `xx', replace force
}

rename hoursworked hours_bus
rename hourlycompensation cph_bus
rename employment emp_bus
rename compensation comp_bus
rename currentdollaroutput output_bus

gen ww_bus = hours_bus/emp_bus / (52 / 1000)
move ww_bus comp_bus

label var output_bus "Output, private business"
label var hours_bus "Hours worked, private business"
label var emp_bus "Employment, private business"
label var comp_bus "Compensation, private business"
label var cph_bus "Compensation per hour, private business"
label var ww_bus "Average weekly hours worked, private business"

merge 1:1 qdate using `index', nogen
move pd_bus output_bus

// add source note
unab foo: *_bus
foreach xx of local foo {
   note `xx': Source: BLS, https://www.bls.gov/lpc/special_requests/nonfarm_business.zip, accessed on `filedate'
}

compress
save ../../data/interim/lpc_data.dta, replace


// PRIVATE NONFARM BUSINESS SECTOR //
tempfile nfb level index

// GET FILE DATE for souce note
insheet using ../../data/raw/nonfarm_business-quarterly-series.date, clear
local filedate = v1[1]


// series in indexes
import excel ../../data/raw/nonfarm_business-quarterly-series.xlsx, clear sheet("NFBUS, All persons (Index)") cellrange(A7) firstrow case(lower)
gen int qdate = yq(year, qtr)

// remove rows with stray headers
keep if qdate != .

keep qdate implicitpricedeflator

// de-string
destring implicitpricedeflator, replace force

rename implicitpricedeflator pd_nfb

label var pd_nfb "Implicit price deflator, private nonfarm business"

save `index'

// series in levels
import excel ../../data/raw/nonfarm_business-quarterly-series.xlsx, clear sheet("NFBUS, All persons (Level)") cellrange(A7) firstrow case(lower)
gen int qdate = yq(year, qtr)
tsset qdate, q
note qdate: Quarter, 1960:Q1 = 1
order qdate

// remove rows with stray headers
keep if qdate != .

keep qdate hoursworked hourlycompensation employment compensation currentdollaroutput

// de-string
foreach xx in hoursworked hourlycompensation employment compensation currentdollaroutput {
   destring `xx', replace force
}

rename hoursworked hours_nfb
rename hourlycompensation cph_nfb
rename employment emp_nfb
rename compensation comp_nfb
rename currentdollaroutput output_nfb

gen ww_nfb = hours_nfb/emp_nfb / (52 / 1000)
move ww_nfb comp_nfb

label var output_nfb "Output, private nonfarm business"
label var hours_nfb "Hours worked, private nonfarm business"
label var emp_nfb "Employment, private nonfarm business"
label var comp_nfb "Compensation, private nonfarm business"
label var cph_nfb "Compensation per hour, private nonfarm business"
label var ww_nfb "Average weekly hours worked, private nonfarm business"

merge 1:1 qdate using `index', nogen
move pd_nfb output_nfb

// add source note
unab foo: *_nfb
foreach xx of local foo {
   note `xx': Source: BLS, https://www.bls.gov/lpc/special_requests/nonfarm_business.zip, accessed on `filedate'
}

compress
save `nfb'


// NONFINANCIAL CORPORATE BUSINESS SECTOR //
tempfile nfcb level index

// GET FILE DATE for souce note
insheet using ../../data/raw/nonfinancial_corporate-quarterly-series.date, clear
local filedate = v1[1]

// series in indexes
import excel ../../data/raw/nonfinancial_corporate-quarterly-series.xlsx, clear sheet("NFC, Employees (Index)") cellrange(A7) firstrow case(lower)
gen int qdate = yq(year, qtr)

// remove rows with stray headers
keep if qdate != .

keep qdate implicitpricedeflator

// de-string
destring implicitpricedeflator, replace force

rename implicitpricedeflator pd_nfcb

label var pd_nfcb "Implicit price deflator, nonfin. corp. business"

save `index'


// series in levels
import excel ../../data/raw/nonfinancial_corporate-quarterly-series.xlsx, clear sheet("NFC, Employees (Level)") cellrange(A7) firstrow case(lower)
gen int qdate = yq(year, qtr)
tsset qdate, q
note qdate: Quarter, 1960:Q1 = 1
order qdate

// remove rows with stray headers
keep if qdate != .

keep qdate hoursworked hourlycompensation employment compensation currentdollaroutput

// de-string
foreach xx in hoursworked hourlycompensation employment compensation currentdollaroutput {
   destring `xx', replace force
}

rename hoursworked hours_nfcb
rename hourlycompensation cph_nfcb
rename employment emp_nfcb
rename compensation comp_nfcb
rename currentdollaroutput output_nfcb

gen ww_nfcb = hours_nfcb/emp_nfcb / (52 / 1000)
move ww_nfcb comp_nfcb

label var output_nfcb "Output, nonfin. corp. business"
label var hours_nfcb "Hours worked, nonfin. corp. business"
label var emp_nfcb "Employment, nonfin. corp. business"
label var comp_nfcb "Compensation, nonfin. corp. business"
label var cph_nfcb "Compensation per hour, nonfin. corp. business"
label var ww_nfcb "Average weekly hours worked, nonfin. corp. business"

merge 1:1 qdate using `index', nogen
move pd_nfcb output_nfcb

// add source note
unab foo: *_nfcb
foreach xx of local foo {
   note `xx': Source: BLS, https://www.bls.gov/lpc/special_requests/nonfinancial_corporate.zip, accessed on `filedate'
}

compress
save `nfcb'


// MERGE TWO SECTORS //
use ../../data/interim/lpc_data.dta, clear
merge 1:1 qdate using `nfb', nogen
merge 1:1 qdate using `nfcb', nogen


// SAVE AND EXIT //
compress
note: Version $ver, created $S_DATE $S_TIME
save ../../data/interim/lpc_data.dta, replace

noi des, f

noi disp as text "$S_DATE $S_TIME  end lpc_data.do"

if "$S_CONSOLE"=="console" & "$batch"!="1" exit, STATA clear
