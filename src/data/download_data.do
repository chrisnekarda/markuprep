// download_data.do
//
// Download data from FRED and other sources
//
// "The Cyclical Behavior of the Price-Cost Markup"
// By Christopher J. Nekarda and Valerie A. Ramey
//
// See instructions in project README. Global environment settings for
// data construction are in build_data.do.
// -------------------------------------------------------------------------- //

noi disp as text "$S_DATE $S_TIME  begin download_data.do"

set fredkey 02c7d5d981e43d2b214de7c440eab00e  // API for paper data

// ------------ //
// MONTHLY DATA //
// ------------ //
// from FRED
noi import fred CE16OV UNEMPLOY ///
   USPRIV CES0500000006 AWHAETP AWHNONAG ///
   FEDFUNDS TB3MS INDPRO TCU IPMANSICS CUMFNS, ///
   clear vintage(21jun2019)
gen mdate = mofd(daten)
tsset mdate, m
order mdate
drop daten datestr

save ../../data/raw/fred_monthly, replace


// --------------------- //
// QUARTERLY MACRO DATA //
// -------------------- //
// from FRED
clear
noi import fred GDPC1 GDPDEF GDP GCEC1 FGCEC1 A132RC1Q027SBEA ///
   B230RC0Q173SBEA A007RA3Q086SBEA ///
   PCND PCESV DNDGRA3Q086SBEA DSERRA3Q086SBEA DSERRG3Q086SBEA ///
   DNDGRG3Q086SBEA Y033RD3Q086SBEA, ///
   clear vintage(21jun2019)
gen qdate = qofd(daten)
tsset qdate, q
note qdate: Quarter, 1960:Q1 = 1
order qdate
drop daten datestr

save ../../data/raw/fred_quarterly, replace

if "$S_OS" != "Windows" {
   cd ../../data/raw

   // -------------------------------- //
   // BLS LABOR PRODUCTIVITY AND COSTS //
   // SPECIAL REQUEST FILES            //
   // -------------------------------- //
   !wget -N https://www.bls.gov/lpc/special_requests/us_total_hrs_emp.xlsx
   !date -r us_total_hrs_emp.xlsx "+%Y%m%d" > us_total_hrs_emp.date

   !wget -N https://www.bls.gov/lpc/special_requests/nonfarm_business.zip
   !unzip -o nonfarm_business.zip
   !date -r nonfarm_business-quarterly-series.xlsx "+%Y%m%d" > nonfarm_business-quarterly-series.date
   !wget -N https://www.bls.gov/lpc/special_requests/nonfinancial_corporate.zip
   !unzip -o nonfinancial_corporate.zip
   !date -r nonfinancial_corporate-quarterly-series.xlsx "+%Y%m%d" > nonfinancial_corporate-quarterly-series.date
   !rm -f nonfarm_business.zip nonfinancial_corporate.zip


   // ------------------ //
   // QUARTERLY TFP DATA //
   // ------------------ //
   // from
   // "A Quarterly, Utilization-Adjusted Series on Total Factor Productivity"
   // by John Fernald (2014)
   !wget -N "https://www.frbsf.org/economic-research/files/quarterly_tfp.xlsx"
   !date -r quarterly_tfp.xlsx "+%Y%m%d" > quarterly_tfp.date


   // ------------- //
   // MARKUP SERIES //
   // ------------- //
   // from
   // "The Rise of Market Power and the Macroeconomic Implications"
   // by De Loecker, Eeckhout, and Unger (2019)
   !wget -N https://sites.google.com/site/deloeckerjan/data-and-code/DLE_markups_fig_v2.csv
   !date -r DLE_markups_fig_v2.csv "+%Y%m%d" > DLE_markups_fig_v2.date


   !wget -N https://www.rbnz.govt.nz/-/media/ReserveBank/Files/Publications/Research/additional-research/leo-krippner/us-monthly-update.xlsx?la=en&revision=b25d2664-c9d9-43c5-b61d-7c4f31e87a1e
   !date -r us-monthly-update.xlsx "+%Y%m%d" > us-monthly-update.date

   cd ../../src/data
}

noi disp as text "$S_DATE $S_TIME  end download_data.do"

if "$S_CONSOLE" == "console" & "$batch" != "1" exit, STATA clear
