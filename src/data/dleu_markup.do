// dleu_markup.do
//
// Process DLEU markup and interpolate annual to quarterly for charts
//
// "The Cyclical Behavior of the Price-Cost Markup"
// By Christopher J. Nekarda and Valerie A. Ramey
//
// See instructions in project README. Global environment settings for
// data construction are in build_data.do.
// -------------------------------------------------------------------------- //

noi disp as text "$S_DATE $S_TIME  begin dleu_markup.do"

insheet using ../../data/raw/DLE_markups_fig_v2.csv, comma clear
rename v1 year
rename v2 mu_cogs

label var mu_cogs "Markup, private business, cost of goods sold"

tsset year, y

// create log index (comparable to NR measures)
gen lmu_cogs = ln(mu_cogs)
sum lmu_cogs if year==2012
replace lmu_cogs = lmu_cogs / r(mean) * 100
label var lmu_cogs "Log markup, private business, cost of goods sold (index, 2012=100)"

// HP filter
tsfilter hp lmu_cogs_hp  = lmu_cogs


// interpolate to quarterly
gen int qdate = yq(year, 2)
tsset qdate, q
note qdate: Quarter, 1960:Q1 = 1
order year qdate
tsfill

ipolate mu_cogs qdate, gen(mu_cogs_q)
label var mu_cogs_q "Markup, private business, cost of goods sold"


ipolate lmu_cogs_hp qdate, gen(lmu_cogs_q_cyc)
label var lmu_cogs_q_cyc "Log markup, private business, cost of goods sold, cyclical component"


// SAVE AND EXIT
compress
note: Version $ver, created $S_DATE $S_TIME
save ../../data/interim/dleu_markup.dta, replace

noi des, f

noi disp as text "$S_DATE $S_TIME  end dleu_markup.do"

if "$S_CONSOLE"=="console" & "$batch"!="1" exit, STATA clear
