// create_markups.do
//
// Construct markup measures
//
// "The Cyclical Behavior of the Price-Cost Markup"
// By Christopher J. Nekarda and Valerie A. Ramey
//
// See instructions in project README. Global environment settings for
// data construction are in build_data.do.
// -------------------------------------------------------------------------- //

noi disp as text "$S_DATE $S_TIME  begin create_markups.do"

use ../../data/interim/markupcyc_data.dta, clear
label data ""


// -------------------------- //
// business sector components //
// -------------------------- //
gen lrgdp = ln(rgdp)
label var lrgdp "Log real GDP"

// real output
gen rout = output_bus / (pd_bus / 100)
label var rout "Real output, business sector"

gen lrout = ln(rout)
label var lrout "Log real output, business sector"

// real compensation
gen lrcomp = ln(comp_bus / (pd_bus / 100))
label var lrcomp "Log real compensation, business sector"

// hours worked
gen lhours = ln(hours_bus)
label var lhours "Log hours worked, business sector"

// compensation per hour (lrcomp = ln + lrcph)
gen lrcph = lrcomp - lhours
label var lrcph "Log real compensation per hour, business sector"

// output per hour (lroutput = lx + ln)
gen lx = lrout - lhours
label var lx "Log real output per hour, business sector"


// --------------- //
// MARKUP MEASURES //
// --------------- //

// CONSTRUCT MARKUPS //
gen mu_bus = output_bus / comp_bus
label var mu_bus "Markup, private business"
note mu_bus: mu_bus = output_bus / comp_bus

gen mu_bus_ws = output_bus / ws_bus
label var mu_bus_ws "Markup using wages and salaries, private business"
note mu_bus_ws: mu_bus_ws = output_bus / ws_bus

gen mu_bus_ws_oh = output_bus / ws_bus_vari
label var mu_bus_ws_oh "Markup using variable wages and salaries, private business"
note mu_bus_ws_oh: mu_bus_ws_oh = output_bus / ws_bus_vari

gen mu_nfb = output_nfb / comp_nfb
label var mu_nfb "Markup, private nonfarm business"
note mu_nfb: mu_nfb = output_nfb / comp_nfb

gen mu_nfcb = output_nfcb / comp_nfcb
label var mu_nfcb "Markup, nonfin. corp. business"
note mu_nfcb: mu_nfcb = output_nfcb / comp_nfcb


// baseline markup is inverse of labor share in private business sector
gen lmu_cd = ln(mu_bus)
label var lmu_cd "Log markup, CD, private business"

gen lmu_cd_ws = ln(mu_bus_ws)
label var lmu_cd_ws "Log markup, CD, wages and salaries, private business"

gen lmu_cd_ws_oh = ln(mu_bus_ws_oh)
label var lmu_cd_ws_oh "Log markup, CD, var. wages and salaries, private business"

gen lmu_cd_nfb = ln(mu_nfb)
label var lmu_cd_nfb "Log markup, CD, private nonfarm business"

gen lmu_cd_nfcb = ln(mu_nfcb)
label var lmu_cd_nfcb "Log markup, CD, nonfin. corp. business"


// ---------------------------------------- //
// CALCULATIONS FOR CES PRODUCTION FUNCTION //
// ---------------------------------------- //

// level of technology estimated from SVAR (see bivartech.do)
merge 1:1 qdate using ../../data/interim/lz_tech_fd, nogen keepusing(lz_tech*)

// mean level of SVAR technology trend
sum lz_tech_fd
local lz_tech = r(mean)

// using Fernald estimates of TFP without utilization adjustment
gen lz_jf = sum(dtfp / 400)
label var lz_jf "Level of technology, using Fernald (2014) TFP"

// rescale lz_jf (unitless) to have same mean as lz_tech
sum lz_jf
local lz_jf = r(mean)
replace lz_jf = lz_jf - `lz_jf' + `lz_tech'

// using Fernald estimates of TFP with utilization adjustment
gen lz_jf_util = sum(dtfp_util / 400)
label var lz_jf_util "Level of technology, using Fernald (2014) util-adj. TFP"

// rescale lz_jf_util (unitless) to have same mean as lz_tech
sum lz_jf_util
local lz_jf_util = r(mean)
replace lz_jf_util = lz_jf_util - `lz_jf_util' + `lz_tech'


// ------------------- //
// based on Y/L (mu_L) //
// ------------------- //

// using z equal to output per hour
gen lmu_ces_yl = lmu_cd + lx
label var lmu_ces_yl "Log markup, CES, mu_L, naive technology trend"

// using z from SVAR technology
gen lmu_ces_yl_zsvar = lmu_cd + (lx - lz_tech)
label var lmu_ces_yl_zsvar "Log markup, CES, mu_L, SVAR technology trend"


// ------------------- //
// based on Y/K (mu_K) //
// ------------------- //

// MS = Shapiro (1986): Estimated elasticity of capital utilization w.r.t output in manuf is 0.3
// uses the estimated elasticity of the workweek to output in manufacturing for all private business
scalar uelast = 0.3
tsfilter hp lrout_cyc = lrout

gen lutil_ms = uelast * lrout_cyc
label var lutil_ms "Log level of captial utilization (Shapiro, 1986)"

gen util_ms = exp(lutil_ms)
label var util_ms "Captial utilization (Shapiro, 1986)"
drop lrout_cyc


// Fernald (2012) utilization (includes both labor and capital)
rename dutil dutil_jf
// create a level from the series of differences
gen lutil_jf = sum(dutil_jf/400)
// want the level to be mean 1, so make log mean zero
sum lutil_jf
replace lutil_jf = lutil_jf-r(mean)
label var lutil_jf "Log level of labor and captial utilization (Fernald, 2012)"
//
gen util_jf = exp(lutil_jf)
label var util_jf "Labor and captial utilization (Fernald, 2012)"


// output/capital ratio
gen yk_bus = rout/rk_privbus
label var yk_bus "Y/K"

egen yk_bus_avg = mean(yk_bus)
label var yk_bus_avg "Y/K, average"


// Re-parameterization as in Cantore-Levine 2012 JEDC //

// elasticity of substitution
gen sigma = 0.5
label var sigma "Elasticity of substitution between capital and labor"

// capital share
//  (The capital share of factor payments is computed from mfptablehis.xlsx,
//   tab "PG" as G/E. Average from 1947 to 2017.)
gen cap_share_avg = 0.3233
label var cap_share_avg "Capital share of factor payments, average"
note cap_share_avg: Computed from data/raw/mfptablehis.xlsx, tab "PG", (col G)/(col E). Average from 1947 to 2017.

// re-parameterize CES distribution parameters to make them unit free
gen alphak = cap_share_avg * yk_bus_avg^(1 - 1/sigma)
label var alphak "re-parameterize CES distribution parameters to make them unit free"


// Markup with CES tech (assumes constant capital utilization)
gen lmu_ces_yk = lmu_cd + ln(1 - alphak * yk_bus^(1/sigma - 1))
label var lmu_ces_yk "Log markup, CES, mu_K, constant capital utilization"

// Markup with CES tech, variable utilization
gen lmu_ces_yk_ums = lmu_cd + ln(1 - alphak * (yk_bus / util_ms)^(1/sigma - 1))
label var lmu_ces_yk_ums "Log markup, CES, mu_K, variable utilization (Shapiro)"

gen lmu_ces_yk_ujf = lmu_cd + ln(1 - alphak * (yk_bus / util_jf)^(1/sigma - 1))
label var lmu_ces_yk_ujf "Log markup, CES, mu_K,  variable utilization (Fernald)"


// ------------------------- //
// MARKUPS W/ OVERHEAD LABOR //
// ------------------------- //
local lmu_ces_list = "yl yl_zsvar yk yk_ums yk_ujf"

// create a version of baseline markup for variable labor sample
clonevar lmu_cd_ws1 = lmu_cd_ws if lmu_cd_ws_oh != .
note lmu_cd_ws1: lmu_cd_ws1 = lmu_cd_ws if lmu_cd_ws_oh != .
order lmu_cd_ws1, after(lmu_cd_ws)

// In logs, these markups just subtract (lmu_cd - lmu_cd_ws_oh).
foreach xx of local lmu_ces_list {
   local label : variable label lmu_ces_`xx'
   gen lmu_ces_oh_`xx' = lmu_ces_`xx' - (lmu_cd - lmu_cd_ws_oh)
   label var lmu_ces_oh_`xx' "`label', var. labor"
}


// ------------------------------- //
// MARKUPS BASED ON MARIGINAL WAGE //
// ------------------------------- //

// merge in estimates of w_M/w_A with 50% premium
merge 1:1 qdate using ../../data/interim/wmwa.dta, nogen keepusing(wmwa_50)

// create marginal markups for these measures
local marg_list = "lmu_cd"

// loop to create marginal markups
foreach xx of local marg_list {
   local label : variable label `xx'
   gen `xx'_marg = `xx' - ln(wmwa_50)
   label var `xx'_marg "`label', marginal wage"
}



// --------------------- //
// SAVE & EXPORT MARKUPS //
// --------------------- //

// this is the final data set, save it now
keep if tin(1947q1, 2017q4)
tsset qdate, q

label data "Data set for 'The Cyclical Behavior of the Price-Cost Markup'"
note drop _dta
note: Version $ver, created $S_DATE $S_TIME
save ../../data/processed/markupcyc_data.dta, replace


// keep only the markup measures //
keep qdate mu* lmu*

label data "Markup measures for 'The Cyclical Behavior of the Price-Cost Markup'"
compress
save ../../data/processed/nekarda_ramey_markups.dta, replace

noi des, f

// EXPORT MARKUPS AS XLSX //
export excel using ../../data/processed/nekarda_ramey_markups.xlsx, replace firstrow(var)

// EXPORT MARKUPS AS CSV //
preserve
gen Date = dofm(mofd(dofq(qdate)) + 3) - 1
format Date %tdCCYYNNDD
order Date
drop qdate
outsheet using ../../data/processed/nekarda_ramey_markups.csv, comma replace
restore

noi disp as text "$S_DATE $S_TIME  end create_markups.do"

if "$S_CONSOLE"=="console" & "$batch"!="1" exit, STATA clear
