// sw2007_irfs_elasticities.do
//
// Smets and Wouters (2007) IRFs and elasticities
//
// "The Cyclical Behavior of the Price-Cost Markup"
// By Christopher J. Nekarda and Valerie A. Ramey
//
// See instructions in project README. Global environment settings for
// data construction are in build_data.do.
// -------------------------------------------------------------------------- //

noi disp as text "$S_DATE $S_TIME  begin sw2007_irfs_elasticities.do"

tempfile money govt tfp ist

import excel ../../data/raw/Smets-Wouters-irfs-elasticities.xlsx, sheet("monetary_shock") firstrow case(lower) clear
rename horizon step
// make monetary shock expansionary
unab foo: *
local bar: subinstr local foo "step" ""
foreach xx of local bar {
   qui replace `xx' = -`xx'
}
keep step y pmarkup
gen shock = "money"
save `money'

import excel ../../data/raw/Smets-Wouters-irfs-elasticities.xlsx, sheet("govt_shock") firstrow case(lower) clear
rename horizon step
keep step y pmarkup
gen shock = "govt"
save `govt'

import excel ../../data/raw/Smets-Wouters-irfs-elasticities.xlsx, sheet("tech_shock") firstrow case(lower) clear
rename horizon step
keep step y pmarkup
gen shock = "tfp"
save `tfp'

import excel ../../data/raw/Smets-Wouters-irfs-elasticities.xlsx, sheet("ist_shock") firstrow case(lower) clear
rename horizon step
keep step y pmarkup
gen shock = "ist"
save `ist'


// merge them together
use `money', clear
merge 1:1 shock step using `govt', nogen update
merge 1:1 shock step using `tfp', nogen update
merge 1:1 shock step using `ist', nogen update


// SAVE AND EXIT
compress
note: Version $ver, created $S_DATE $S_TIME
save ../../data/interim/sw2007_irfs_elasticities, replace

noi des, f

noi disp as text "$S_DATE $S_TIME  end sw2007_irfs_elasticities.do"

if "$S_CONSOLE"=="console" & "$batch"!="1" exit, STATA clear
