// quarterly_tfp.do
//
// Process Fernald's (2014) quarterly TFP data
//
// "The Cyclical Behavior of the Price-Cost Markup"
// By Christopher J. Nekarda and Valerie A. Ramey
//
// See instructions in project README. Global environment settings for
// data construction are in build_data.do.
// -------------------------------------------------------------------------- //

noi disp as text "$S_DATE $S_TIME  begin quarterly_tfp.do"


// GET FILE DATE for souce note
insheet using ../../data/raw/quarterly_tfp.date, clear
local filedate = v1[1]


// READ IN .XLSX FILE
import excel ../../data/raw/quarterly_tfp.xlsx, sheet("quarterly") cellrange(A2:V2000) firstrow case(lower) clear

// FIX FORMATTING, add source note
unab foo: *
local bar: subinstr local foo "date" ""
foreach vv of local bar {
   format `vv' %10.0g
   note `vv': Source: Fernald (2014), https://www.frbsf.org/economic-research/files/quarterly_tfp.xlsx, accessed on `filedate'
}
gen int qdate=quarterly(date,"YQ")
drop if qdate==.
drop date
order qdate
tsset qdate, q
note qdate: Quarter, 1960:Q1 = 1


// LABLE VARIABLES
label var dk "Capital input (400 * d ln)"
label var dtfp "Business sector TFP growth (400 * d ln)"
label var dutil "Utilization of capital and labor (400 * d ln)"
label var dtfp_util "Business sector TFP growth, adjusted for utilization (400 * d ln)"
label var dlq "Labor quality growth (400 * d ln)"

// we use alpha for labor
rename alpha capshare
label var capshare "Capital's share of income"

// log level of capital services
gen lks = sum(dk/400)
label var lks "Level of capital services (log)"

// SAVE AND EXIT
compress
note: Version $ver, created $S_DATE $S_TIME
save ../../data/interim/quarterly_tfp, replace

noi des, f

noi disp as text "$S_DATE $S_TIME  end quarterly_tfp.do"

if "$S_CONSOLE"=="console" & "$batch"!="1" exit, STATA clear
