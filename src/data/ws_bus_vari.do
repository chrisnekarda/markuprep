// ws_bus_vari.do
//
// Create a measure of variable compensation (i.e., excluding overhead labor)
//
// "The Cyclical Behavior of the Price-Cost Markup"
// By Christopher J. Nekarda and Valerie A. Ramey
//
// See instructions in project README. Global environment settings for
// data construction are in master.do.
// -------------------------------------------------------------------------- //

noi disp as text "$S_DATE $S_TIME  begin ws_bus_vari.do"


// MONTHLY DATA FROM FRED //
use ../../data/interim/fred_monthly.dta, clear


// annual hours for CES private
gen hrs_priv = emp_priv * ww_priv * 52 / 1000
label var hrs_priv "Annual hours, all employees"
note hrs_priv: hrs_priv = emp_priv * ww_priv * 52 / 1000

// annual hours for CES private production workers
gen hrs_priv_prod = emp_priv_prod * ww_priv_prod * 52 / 1000
label var hrs_priv_prod "Annual hours, production and nonsupervisory workers"
note hrs_priv_prod: hrs_priv_prod = emp_priv_prod * ww_priv_prod * 52 / 1000

/*
// ratio of production worker employment to total employment
gen ratio_emp = emp_priv_prod / emp_priv

// ratio of workweeks of production workers to workweeks of all workers
// (only available from 2006 forward)
gen ratio_ww = ww_priv_prod / ww_priv

// ratio of total hours of production workers to total hours of all workers
// (only available from 2006 forward)
gen ratio_hrs = hrs_priv_prod / hrs_priv
*/


// merge in LPC data
merge 1:1 qdate using ../../data/interim/lpc_data, keepusing(hours_bus) nogen

// merge in wages and salaries
merge 1:1 qdate using ../../data/interim/fred_quarterly, keepusing(ws_bus) nogen


// CREATE VARIABLES //

// fraction of overhead labor (using fraction of CES employment)
gen nu = emp_priv_prod / emp_priv
label var nu "Fraction of variable hours in total hours"
note nu: nu = emp_priv_prod / emp_priv

// business sector variable compensation (wages and salaries divided by hours)
gen ahe_bus = ws_bus/hours_bus
label var ahe_bus "Average hourly earnings, private business"
note ahe_bus: ahe_bus = ws_bus/hours_bus

// business sector variable hours
gen hours_bus_vari = nu * hours_bus
label var hours_bus_vari "Hours worked, private business, variable labor"
note hours_bus_vari: hours_bus_vari = nu * hours_bus

// business sector variable compensation
gen ws_bus_vari = hours_bus_vari * ahe_bus
label var ws_bus_vari "Wages and salaries, private business, variable labor"
note ws_bus_vari: ws_bus_vari = hours_bus_vari * ahe_bus


// SAVE AND EXIT
keep qdate nu ahe_bus hours_bus_vari ws_bus_vari *priv*
keep if tin(1947q1, 2017q4)

compress
note: Version $ver, created $S_DATE $S_TIME
save ../../data/interim/ws_bus_vari.dta, replace

noi des, f

noi disp as text "$S_DATE $S_TIME  end ws_bus_vari.do"

if "$S_CONSOLE"=="console" & "$batch"!="1" exit, STATA clear
