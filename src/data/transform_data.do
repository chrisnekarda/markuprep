// transform_data.do
//
// Minor transformations of raw data from FRED
//
// "The Cyclical Behavior of the Price-Cost Markup"
// By Christopher J. Nekarda and Valerie A. Ramey
//
// See instructions in project README. Global environment settings for
// data construction are in build_data.do.
// -------------------------------------------------------------------------- //

noi disp as text "$S_DATE $S_TIME  begin transform_data.do"


// ----------------- //
// MONTHLY FRED DATA //
// ----------------- //

use ../../data/raw/fred_monthly.dta, clear

gen ur = 100 * UNEMPLOY / (UNEMPLOY + CE16OV)

gen emp_priv = USPRIV / 1000
gen emp_priv_prod = CES0500000006 / 1000
rename AWHAET ww_priv
rename AWHNONAG ww_priv_prod

rename FEDFUNDS rff
rename TB3MS tb3
rename INDPRO jqi_tot
rename TCU rku_tot
rename IPMANSICS jqi_mfg
rename CUMFNS rku_mfg

// collapse to quarterly
tscollap ur *priv* rff tb3 jqi* rku*, to(q) gen(qdate)
order qdate
note qdate: Quarter, 1960:Q1 = 1

// label variables
label var ur "Unemployment rate"
note ur: Source: FRED, https://fred.stlouisfed.org/series/UNEMPLOY and https://fred.stlouisfed.org/series/CE16OV
note ur: ur = 100 * UNEMPLOY / (UNEMPLOY + CE16OV)

label var emp_priv "Employment, all employees"
note emp_priv: Source: FRED, https://fred.stlouisfed.org/series/USPRIV
note emp_priv: emp_priv = USPRIV / 1000

label var emp_priv_prod "Employment, production and nonsupervisory workers"
note emp_priv_prod: Source: FRED, https://fred.stlouisfed.org/series/CES0500000006
note emp_priv_prod: emp_priv_prod = CES0500000006 / 1000

label var ww_priv "Average weekly hours, all employees"
note ww_priv: Source: FRED, https://fred.stlouisfed.org/series/AWHAET

label var ww_priv_prod "Average weekly hours, production and nonsupervisory workers"
note ww_priv_prod: Source: FRED, https://fred.stlouisfed.org/series/AWHNONAG

label var rff "Effective federal funds rate"
note rff: Source: FRED, https://fred.stlouisfed.org/series/FEDFUNDS

label var tb3 "3-month Treasury bill rate"
note tb3: Source: FRED, https://fred.stlouisfed.org/series/TB3MS

label var jqi_tot "Industrial production, total industry (Index, 2012=100)"
note jqi_tot: Source: FRED, https://fred.stlouisfed.org/series/INDPRO

label var jqi_mfg "Index of industrial production, manufacturing (Index, 2012=100)"
note jqi_mfg: Source: FRED, https://fred.stlouisfed.org/series/IPMANSICS

label var rku_tot "Capacity utilization, total industry (percent of capacity)"
note rku_tot: Source: FRED, https://fred.stlouisfed.org/series/TCU

label var rku_mfg "Capacity utilization, manufacturing (percent of capacity)"
note rku_mfg: Source: FRED, https://fred.stlouisfed.org/series/CUMFNS

compress
note: Version $ver, created $S_DATE $S_TIME
save ../../data/interim/fred_monthly.dta, replace

noi des, f


// ----------------------- //
// RAMEY MILITARY SPENDING //
// ----------------------- //

import excel ../../data/raw/pdvmil.xlsx, firstrow clear

// create date variable
gen int qdate = quarterly(date,"YQ")
drop if qdate == .
tsset qdate, q
note qdate: Quarter, 1960:Q1 = 1
drop date

label var pdvmil "Present value of change in expected defense spending due to political events"
note pdvmil: Source: Ramey (2012), updated.

compress
note: Version $ver, created $S_DATE $S_TIME
save ../../data/interim/pdvmil.dta, replace

noi des, f


// ------------------- //
// QUARTERLY FRED DATA //
// ------------------- //

use ../../data/raw/fred_quarterly.dta, clear

rename GDPC1 rgdp
label var rgdp "Real Gross Domestic Product"
note rgdp: Source: FRED, https://fred.stlouisfed.org/series/GDPC1

rename GDPDEF pgdp
label var pgdp "Gross Domestic Product: Implicit Price Deflator"
note pgdp: Source: FRED, https://fred.stlouisfed.org/series/GDPDEF

rename GDP ngdp
label var ngdp "Gross Domestic Product"
note ngdp: Source: FRED, https://fred.stlouisfed.org/series/GDP

rename GCEC1 rgov
label var rgov "Real Government Consumption Expenditures and Gross Investment"
note rgov: Source: FRED, https://fred.stlouisfed.org/series/GCEC1

rename FGCEC1 rfed
label var rfed "Real Federal Consumption Expenditures and Gross Investment"
note rfed: Source: FRED, https://fred.stlouisfed.org/series/FGCEC1

rename A132RC1Q027SBEA ws_bus
label var ws_bus "Wages and salaries, private business"
note ws_bus: Source: FRED, https://fred.stlouisfed.org/series/A132RC1Q027SBEA

rename B230RC0Q173SBEA pop
label var pop  "Population"
note pop: Source: FRED, https://fred.stlouisfed.org/series/B230RC0Q173SBEA

rename A007RA3Q086SBEA rinvest_priv_fixed
label var rinvest_priv_fixed "Real gross private domestic investment: Fixed investment"
note rinvest_priv_fixed: Source: FRED, https://fred.stlouisfed.org/series/A007RA3Q086SBEA

rename PCND pcend
label var pcend "Personal Consumption Expenditures: Nondurable Goods"
note pcend: Source: FRED, https://fred.stlouisfed.org/series/PCND

rename PCESV pcesv
label var pcesv "Personal Consumption Expenditures: Services"
note pcesv: Source: FRED, https://fred.stlouisfed.org/series/PCESV

rename DNDGRA3Q086SBEA qrpcend
label var qrpcend "Quantity index, real personal consumption expenditures: Nondurable goods"
note qrpcend: Source: FRED, https://fred.stlouisfed.org/series/DNDGRA3Q086SBEA

rename DSERRA3Q086SBEA qrpcesv
label var qrpcesv "Quantity index, real personal consumption expenditures: Services"
note qrpcesv: Source: FRED, https://fred.stlouisfed.org/series/DSERRA3Q086SBEA

rename DNDGRG3Q086SBEA ppcend
label var ppcend "Price index, personal consumption expenditures: Nondurable goods"
note ppcend: Source: FRED, https://fred.stlouisfed.org/series/DNDGRG3Q086SBEA

rename DSERRG3Q086SBEA ppcesv
label var ppcesv "Price index, personal consumption expenditures: Services"
note ppcesv: Source: FRED, https://fred.stlouisfed.org/series/DNDGRG3Q086SBEA

rename Y033RD3Q086SBEA pequip
label var pequip "Price index, nonresidential investment: Equipment"
note pequip: Source: FRED, https://fred.stlouisfed.org/series/Y033RD3Q086SBEA



compress
note: Version $ver, created $S_DATE $S_TIME
save ../../data/interim/fred_quarterly.dta, replace

noi des, f

noi disp as text "$S_DATE $S_TIME  end transform_data.do"

if "$S_CONSOLE"=="console" & "$batch"!="1" exit, STATA clear
