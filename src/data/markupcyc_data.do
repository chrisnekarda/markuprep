// markupcyc_data.do
//
// Assemble external data (i.e., apart from markups)
//
// "The Cyclical Behavior of the Price-Cost Markup"
// By Christopher J. Nekarda and Valerie A. Ramey
//
// See instructions in project README. Global environment settings for
// data construction are in build_data.do.
// -------------------------------------------------------------------------- //

noi disp as text "$S_DATE $S_TIME  begin build_data.do"

use ../../data/interim/fred_quarterly, clear

// merge in interim sources //
merge 1:1 qdate using ../../data/interim/fred_monthly, nogen
merge 1:1 qdate using ../../data/interim/lpc_data, nogen
merge 1:1 qdate using ../../data/interim/ws_bus_vari, nogen
merge 1:1 qdate using ../../data/interim/quarterly_tfp, nogen keepusing(dtfp dutil dtfp_util)
merge 1:1 qdate using ../../data/interim/kstock, nogen keepusing(rk_privbus)

// variables for conditional cyclicality //
merge 1:1 qdate using ../../data/interim/krippner_ssr, nogen
merge 1:1 qdate using ../../data/interim/pdvmil, nogen
merge 1:1 qdate using ../../data/interim/pcendsv, nogen
merge 1:1 qdate using ../../data/raw/pzall, nogen  // !!! make sure to replace this with noise !!!

sort qdate

// define Ramey defense news variable
gen pdvmil_ngdp = 100 * pdvmil / L.ngdp
label var pdvmil_ngdp "Defense news (percent of lagged nominal GDP)"
note pdvmil_ngdp: Source: Ramey (2012) and BEA.

/*
// order variables
#delim ;
order
qdate
rgdp
ngdp
pgdp

output_bus
pd_bus
hours_bus
emp_bus
ww_bus
comp_bus
cph_bus
ws_bus

output_nfb
pd_nfb
hours_nfb
emp_nfb
ww_nfb
comp_nfb
cph_nfb

output_nfcb
pd_nfcb
hours_nfcb
emp_nfcb
ww_nfcb
comp_nfcb
cph_nfcb

pop
ur
rff
ssr
tb3
jqi_tot
rku_tot
jqi_mfg
rku_mfg
pdvmil
pdvmil_ngdp
rgov
rfed

dtfp
dutil
dtfp_util
;
#delim cr
*/
order qdate

// sample selection for paper
keep if tin(1947q1, 2017q4)
noi tsset


label data "External data for 'The Cyclical Behavior of the Price-Cost Markup'"
note: Version $ver, created $S_DATE $S_TIME
order qdate

compress
save ../../data/interim/markupcyc_data, replace

noi des, f

noi disp as text "$S_DATE $S_TIME  end markupcyc_data.do"

if "$S_CONSOLE"=="console" & "$batch"!="1" exit, STATA clear
