// krippner_ssr
//
// Process Krippner SSR data and convert to quarterly
//
// "The Cyclical Behavior of the Price-Cost Markup"
// By Christopher J. Nekarda and Valerie A. Ramey
//
// See instructions in project README. Global environment settings for
// data construction are in build_data.do.
// -------------------------------------------------------------------------- //

noi disp as text "$S_DATE $S_TIME  begin krippner_ssr"

import excel ../../data/raw/us-monthly-update.xlsx, sheet("D. Monthly average") cellrange(A6) firstrow case(lower) clear

gen date = date(a, "DMY")
gen int mdate = mofd(date)
tsset mdate, m

tscollap ssr, to(q) gen(qdate)
order qdate
note qdate: Quarter, 1960:Q1 = 1
drop if ssr == .


label var ssr "Krippner shadow short rate (SSR)"
note ssr: Source: Krippner (2013). Monthly average.


// SAVE AND EXIT
compress
note: Version $ver, created $S_DATE $S_TIME
save ../../data/interim/krippner_ssr.dta, replace

noi des, f

noi disp as text "$S_DATE $S_TIME  end krippner_ssr"

if "$S_CONSOLE"=="console" & "$batch"!="1" exit, STATA clear
