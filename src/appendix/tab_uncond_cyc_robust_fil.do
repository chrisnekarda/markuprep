// tab_uncond_cyc_robust_fil.do
//
// Results reported in table 1: Unconditional cyclicality
//
// "The Cyclical Behavior of the Price-Cost Markup"
// By Christopher J. Nekarda and Valerie A. Ramey
//
// See instructions in project README.
//
// -------------------------------------------------------------------------- //

est drop _all
global oldfil = "$fil"

// variable for date-time stamp
local d = string(clock("`c(current_date)' `c(current_time)'", "DMY hms"), "%tcCCYYNNDD!_HHMM")

cap log close
log using ../../log/tab_uncond_cyc_robust_fil_`d'.log, text replace

noi disp as text "$S_DATE $S_TIME  begin tab_uncond_cyc_robust_fil.do"


// list of variables to estimate elasticity
local varlist = "lmu_cd lmu_cd_ws lmu_cd_ws1 lmu_cd_ws_oh lmu_ces_yl lmu_ces_yl_zsvar lmu_ces_yk lmu_ces_yk_ums lmu_ces_yk_ujf lmu_ces_oh_yl lmu_ces_oh_yl_zsvar lmu_ces_oh_yk lmu_ces_oh_yk_ums lmu_ces_oh_yk_ujf"

// optional start and ending dates for all variables, estbeg will not affect
// the variables with a shorter sample
local estbeg = tq(1947q1)
local estend = tq(2017q4)

foreach fil in bk fd {
   use ../../data/interim/markups_cyc_`fil'.dta, clear

   rename lrgdp_cyc lrgdp_`fil'

   // loop for regressions
   local i 0
   foreach xx of local varlist {
      local i `++i'
      ivreg2 `xx'_cyc lrgdp_`fil' if qdate>=`estbeg' & qdate<=`estend', bw(auto)
      eststo est`i'_`fil'
   }
}

// construct a list of variables as numbered in the table
local tablist
forvalues i = 1/14 {
   local tablist = "`tablist' est`i'_bk est`i'_fd"
}

// re-arrange estout matrixes to produce a table by rows
qui esttab `tablist', se nostar r2 drop(_cons)
matrix C = r(coefs)
eststo clear
local rnames : rownames C
local models : coleq C
local models : list uniq models
local i 0
foreach name of local rnames {
   local ++i
   local j 0
   capture matrix drop b
   capture matrix drop se
   foreach model of local models {
      local ++j
      matrix tmp = C[`i', 2 * `j' - 1]
      if tmp[1, 1]<. {
            matrix colnames tmp = `model'
            matrix b = nullmat(b), tmp
            matrix tmp[1, 1] = C[`i', 2 * `j']
            matrix se = nullmat(se), tmp
      }
   }
   ereturn post b
   quietly estadd matrix se
   eststo `name'
}

// report the table
noi disp _n "Elasticity of detrended markup w.r.t. detrended real GDP"
noi esttab, b(%4.2f) wide se star mtitle noobs compress nonumb

noi disp as text "$S_DATE $S_TIME  end tab_uncond_cyc_robust_fil.do"

cap log close

if "$S_CONSOLE"=="console" & "$batch"!="1" exit, STATA clear
