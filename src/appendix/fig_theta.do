// fig_theta.do
//
// Figures for alternative markups - robustness
//
// "The Cyclical Behavior of the Price-Cost Markup"
// By Christopher J. Nekarda and Valerie A. Ramey
//
// See instructions in project README.
//
// -------------------------------------------------------------------------- //

noi disp as text "$S_DATE $S_TIME  begin fig_theta.do"


use ../../data/raw/pct-ovt-ann.dta, clear
tsset year, y
drop if year<1969

gen theta_may_cps = 100 * pct_overtime if year <= 1981
gen theta_bls_cost = 100 * novt / ((v_ann / h_ann) * nws * .5) if year > 1982

// ----- //
// theta //
// ----- //

#delim ;
tw
(line theta_may_cps year, yaxis(1 2) lc("$c7"))
(line theta_bls_cost year, yaxis(1 2) lc("$c8"))
,
ttitle("")
subtitle("Fraction", pos(11))
legend(order(1 "CPS May extract" 2 "ECEC") bm(4 0 0 2))
ylab(20(5)40, for(%-6.0f)) ymtick(40, nolab tlen(4.5))
ylab(20(5)40, for(%-6.0f) axis(2)) ymtick(40, axis(2) nolab tlen(4.5))
ytitle("") ytitle("", axis(2))
yline(30, lp(dash) lc("$c6"))
xtick(1967.5(1)2011.5)
xlab(1970(5)2010, notick)
xline(
1960.25(.025)1961.00
1969.75(.025)1970.75
1973.75(.025)1975.00
1980.00(.025)1980.50
1981.50(.025)1982.75
1990.50(.025)1991.00
2001.00(.025)2001.75
2007.75(.025)2009.25
, lc("$c16") lw(medium))
scheme(texpdf)
name(theta, replace)
;
#delim cr
graph export ../../output/appendix/fig_theta.pdf, replace

noi disp as text "$S_DATE $S_TIME  end fig_theta.do"

if "$S_CONSOLE"=="console" & "$batch"!="1" exit, STATA clear
