// fig_mu_robust.do
//
// Figures for alternative markups - robustness
//
// "The Cyclical Behavior of the Price-Cost Markup"
// By Christopher J. Nekarda and Valerie A. Ramey
//
// See instructions in project README.
//
// -------------------------------------------------------------------------- //

noi disp as text "$S_DATE $S_TIME  begin fig_mu_robust.do"

use ../../data/processed/markupcyc_data.dta, clear
merge 1:1 qdate using ../../data/interim/dleu_markup, nogen keepusing(mu_cogs_q)


// --------------------- //
// markup chart - levels //
// --------------------- //

// create index variable
foreach xx in mu_bus mu_nfb mu_nfcb mu_cogs_q {
   sum `xx' if tin(2012q1, 2012q4)
   gen i_`xx' = `xx' / r(mean) * 100
}


#delim ;
tw
(line i_mu_cogs_q qdate, yaxis(1 2) lc("$c10"))
(line i_mu_nfcb qdate, yaxis(1 2) lc("$c9"))
(line i_mu_nfb qdate, yaxis(1 2) lc("$c8"))
(line i_mu_bus qdate, yaxis(1 2) lc("$c7"))
,
ttitle("")
subtitle("Index, 2012 = 100", pos(11))
legend(order(4 "Private business, inverse of labor share" 1 "Private business, cost of goods sold" 3 "Private nonfarm business" 2 "Nonfinancial corporate business" ) bm(4 0 0 2))
ylab(80(5)110, for(%-6.0f)) ymtick(110, nolab tlen(4.5))
ylab(80(5)110, axis(2) for(%-6.0f)) ymtick(110, axis(2) nolab tlen(4.5))
xtick(`=tq(1947q1)-4.5'(4)`=tq(2017q4)+4.5')
xlab(`=tq(1950q3)-.5'(20)`=tq(2015q3)-.5', notick for(%tqCCYY))
xline(
`=tq(1948q4)-.5'(.25)`=tq(1949q4)-.5'
`=tq(1953q3)-.5'(.25)`=tq(1954q2)-.5'
`=tq(1957q3)-.5'(.25)`=tq(1958q2)-.5'
`=tq(1960q2)-.5'(.25)`=tq(1961q1)-.5'
`=tq(1969q4)-.5'(.25)`=tq(1970q4)-.5'
`=tq(1973q4)-.5'(.25)`=tq(1975q1)-.5'
`=tq(1980q1)-.5'(.25)`=tq(1980q3)-.5'
`=tq(1981q3)-.5'(.25)`=tq(1982q4)-.5'
`=tq(1990q3)-.5'(.25)`=tq(1991q1)-.5'
`=tq(2001q1)-.5'(.25)`=tq(2001q4)-.5'
`=tq(2007q4)-.5'(.25)`=tq(2009q2)-.5'
, lc("$c16") lw(medium))
scheme(texpdf)
name(markups, replace)
;
#delim cr
graph export ../../output/appendix/fig_mu_robust_lev.pdf, replace


// ---------------------- //
// detrended markup chart //
// ---------------------- //

foreach xx in mu_bus mu_nfb mu_nfcb mu_cogs_q {
   cap gen l`xx' = ln(`xx')
   tsfilter hp l`xx'_cyc = l`xx'
   qui replace l`xx'_cyc = 100 * l`xx'_cyc
}

#delim ;
tw
(line lmu_nfcb_cyc qdate, yaxis(1 2) lc("$c9"))
(line lmu_nfb_cyc qdate, yaxis(1 2) lc("$c8"))
(line lmu_cogs_q_cyc qdate, yaxis(1 2) lc("$c10"))
(line lmu_bus_cyc qdate, yaxis(1 2) lc("$c7"))
,
ttitle("")
subtitle("Percent deviation from trend", pos(11))
legend(order(4 "Private business, inverse of labor share" 3 "Private business, cost of goods sold" 2 "Private nonfarm business" 1 "Nonfinancial corporate business") col(2) row(2) bm(4 0 0 2))
ylab(-5(1)5, for(%-4.0f)) ymtick(5, nolab tlen(4.5))
ylab(-5(1)5, axis(2) for(%-4.0f)) ymtick(5, axis(2) nolab tlen(4.5))
yline(0, lc(gs0) lw(medthin))
xtick(`=tq(1947q1)-4.5'(4)`=tq(2017q4)+4.5')
xlab(`=tq(1950q3)-.5'(20)`=tq(2015q3)-.5', notick for(%tqCCYY))
xline(
`=tq(1948q4)-.5'(.25)`=tq(1949q4)-.5'
`=tq(1953q3)-.5'(.25)`=tq(1954q2)-.5'
`=tq(1957q3)-.5'(.25)`=tq(1958q2)-.5'
`=tq(1960q2)-.5'(.25)`=tq(1961q1)-.5'
`=tq(1969q4)-.5'(.25)`=tq(1970q4)-.5'
`=tq(1973q4)-.5'(.25)`=tq(1975q1)-.5'
`=tq(1980q1)-.5'(.25)`=tq(1980q3)-.5'
`=tq(1981q3)-.5'(.25)`=tq(1982q4)-.5'
`=tq(1990q3)-.5'(.25)`=tq(1991q1)-.5'
`=tq(2001q1)-.5'(.25)`=tq(2001q4)-.5'
`=tq(2007q4)-.5'(.25)`=tq(2009q2)-.5'
, lc("$c16") lw(medium))
scheme(texpdf)
name(mu_robust_cyc, replace)
;
#delim cr
graph export ../../output/appendix/fig_mu_robust_cyc.pdf, replace


noi disp as text "$S_DATE $S_TIME  end fig_mu_robust.do"

if "$S_CONSOLE"=="console" & "$batch"!="1" exit, STATA clear
