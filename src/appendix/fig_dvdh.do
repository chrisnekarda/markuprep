// fig_dvdh.do
//
// Figures for alternative markups - robustness
//
// "The Cyclical Behavior of the Price-Cost Markup"
// By Christopher J. Nekarda and Valerie A. Ramey
//
// See instructions in project README.
//
// -------------------------------------------------------------------------- //

noi disp as text "$S_DATE $S_TIME  begin fig_dvdh.do"

use ../../data/interim/wmwa.dta, clear


// ------------- //
// v/h and dv/dh //
// ------------- //
#delim ;
tw
(line dvdh qdate, yaxis(1) lc("$c7"))
(line vh qdate, yaxis(2) lc("$c8"))
,
ttitle("")
subtitle(" ", pos(11))
legend(order(1 "{it:dv}/{it:dh} (left scale)" 2 "{it:v}/{it:h} (right scale)") bm(4 0 0 2))
ylab(.40(.04).60, for(%-6.2f)) ymtick(.60, nolab tlen(4.5))
ylab(.08(.01).13, axis(2) for(%-6.2f)) ymtick(.13, axis(2) nolab tlen(4.5))
ytitle("") ytitle("", axis(2))
xtick(`=tq(1976q1)-4.5'(4)`=tq(2017q4)+4.5')
xlab(`=tq(1980q3)-.5'(20)`=tq(2015q3)-.5', notick for(%tqCCYY))
xline(
`=tq(1980q1)-.5'(.125)`=tq(1980q3)-.5'
`=tq(1981q3)-.5'(.125)`=tq(1982q4)-.5'
`=tq(1990q3)-.5'(.125)`=tq(1991q1)-.5'
`=tq(2001q1)-.5'(.125)`=tq(2001q4)-.5'
`=tq(2007q4)-.5'(.125)`=tq(2009q2)-.5'
, lc("$c16") lw(medium))
scheme(texpdf)
name(vh, replace)
;
#delim cr
graph export ../../output/appendix/fig_dvdh_vh.pdf, replace


// ------- //
// w_m/w_a //
// ------- //
#delim ;
tw
(line wmwa qdate, yaxis(1 2) lc("$c7"))
,
ttitle("")
subtitle(" ", pos(11))
legend(off)
ylab(1.04(.01)1.07, for(%-6.2f))
ylab(1.04(.01)1.07, axis(2) for(%-6.2f))
ytitle("") ytitle("", axis(2))
xtick(`=tq(1976q1)-4.5'(4)`=tq(2017q4)+4.5')
xlab(`=tq(1980q3)-.5'(20)`=tq(2015q3)-.5', notick for(%tqCCYY))
xline(
`=tq(1980q1)-.5'(.125)`=tq(1980q3)-.5'
`=tq(1981q3)-.5'(.125)`=tq(1982q4)-.5'
`=tq(1990q3)-.5'(.125)`=tq(1991q1)-.5'
`=tq(2001q1)-.5'(.125)`=tq(2001q4)-.5'
`=tq(2007q4)-.5'(.125)`=tq(2009q2)-.5'
, lc("$c16") lw(medium))
scheme(texpdf)
name(vh, replace)
;
#delim cr
graph export ../../output/appendix/fig_wmwa.pdf, replace

noi disp as text "$S_DATE $S_TIME  end fig_dvdh.do"

if "$S_CONSOLE"=="console" & "$batch"!="1" exit, STATA clear
