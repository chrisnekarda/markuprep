// extract_irfs_robust.do
//
// Extract IRF data for appendix figures
//
// "The Cyclical Behavior of the Price-Cost Markup"
// By Christopher J. Nekarda and Valerie A. Ramey
//
// See instructions in project README.
//
// -------------------------------------------------------------------------- //

// this file must be executed with -run-; it will not work correctly using -do-.

noi disp as text "$S_DATE $S_TIME  begin extract_irfs_robust.do"


set linesize 255

local varlist = "lmu_cd lmu_ces_yk_ums"

cap erase ../../output/appendix/oirf.dta
foreach xx of local varlist {

   // MONETARY POLICY SHOCK -- BASELINE //
   clear
   tempfile log dat
   cap log close
   log using `log', text replace
   noi irf table oirf, set(../../est/irf_`xx'.irf) irf(moneyvar) level(90) stde impulse(ssr) response(lrgdpc `xx' lp lpcom)
   log close

   tempname fhr fhw
   local linenum = 0
   file open `fhr' using `log', read
   file open `fhw' using `dat', write replace
   file read `fhr' line
   while r(eof)==0 {
      local linenum = `linenum' + 1
      if `linenum'>=9 & `linenum'<=29 {
         file write `fhw' `"`macval(line)'"' _n
      }
      file read `fhr' line
   }
   file close `fhr'
   file close `fhw'

   infix step 2-5 double lrgdp 12-19 double lrgdp_p05 24-31 double lrgdp_p95 36-42 double lrgdp_se 48-54 ///
         double lmu 60-67 double lmu_p05 72-79 double lmu_p95 84-90 double lmu_se 96-102 ///
         double lplev 108-115 double lplev_p05 120-127 double lplev_p95 132-139 double lplev_se 144-151 ///
         double lpcom 156-163 double lpcom_p05 168-175 double lpcom_p95 180-187 double lpcom_se 192-199 using `dat', clear
   drop if step==.
   tsset step
   compress
   aorder
   order step
   gen shock = "money"
   gen markup = "`xx'"
   cap confirm file ../../output/appendix/oirf.dta
   if _rc==0 {
      append using ../../output/appendix/oirf.dta
   }
   save ../../output/appendix/oirf.dta, replace


   // MONETARY POLICY SHOCK -- ALTERNATE ORDERING //
   clear
   tempfile log dat
   cap log close
   log using `log', text replace
   noi irf table oirf, set(../../est/irf_robust_`xx'.irf) irf(moneyvar_alt) ///
      level(90) stde impulse(ssr)  response(lrgdpc `xx' lp lpcom)
   log close

   tempname fhr fhw
   local linenum = 0
   file open `fhr' using `log', read
   file open `fhw' using `dat', write replace
   file read `fhr' line
   while r(eof)==0 {
      local linenum = `linenum' + 1
      if `linenum'>=9 & `linenum'<=29 {
         file write `fhw' `"`macval(line)'"' _n
      }
      file read `fhr' line
   }
   file close `fhr'
   file close `fhw'

   infix step 2-5 double lrgdp 12-19 double lrgdp_p05 24-31 double lrgdp_p95 36-42 double lrgdp_se 48-54 ///
      double lmu 60-67 double lmu_p05 72-79 double lmu_p95 84-90 double lmu_se 96-102 ///
      double lplev 108-115 double lplev_p05 120-127 double lplev_p95 132-139 double lplev_se 144-151 ///
      double lpcom 156-163 double lpcom_p05 168-175 double lpcom_p95 180-187 double lpcom_se 192-199 using `dat', clear
   drop if step==.
   tsset step
   compress
   aorder
   order step
   gen shock = "money_alt"
   gen markup = "`xx'"
   cap confirm file ../../output/appendix/oirf.dta
   if _rc==0 {
      append using ../../output/appendix/oirf.dta
   }
   save ../../output/appendix/oirf.dta, replace

}

gen byte line = 1 if markup=="lmu_cd"

order line markup shock step
sort line shock step
compress
save ../../output/appendix/oirf.dta, replace


noi disp as text "$S_DATE $S_TIME  end extract_irfs_robust.do"

if "$S_CONSOLE"=="console" & "$batch"!="1" exit, STATA clear
