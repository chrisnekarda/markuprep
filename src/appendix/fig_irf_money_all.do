// fig_irf_money_all.do
//
// Figures for all monetary IRFs
//
// "The Cyclical Behavior of the Price-Cost Markup"
// By Christopher J. Nekarda and Valerie A. Ramey
//
// See instructions in project README.
//
// -------------------------------------------------------------------------- //

noi disp as text "$S_DATE $S_TIME  begin fig_irf_money_all.do"

use ../../output/appendix/oirf.dta, clear

// only plotting these two measures
keep if markup == "lmu_cd" | markup == "lmu_ces_yk_ums"

// scale for humans (multiply by 100)
unab foo: lmu* lrgdp* lplev* lpcom*
foreach xx of local foo {
   replace `xx' = 100 * `xx'
   rename `xx' `xx'_
}

drop line
reshape wide lmu* lrgdp* lplev* lpcom*, i(shock step) j(markup) s


// make the stata shocks start in period 1
expand 2, gen(exp)
drop if exp==1 & step>0
replace step = step+1 if exp==0
drop if step>20
drop exp
unab foo: lmu* lrgdp* lplev* lpcom*
foreach xx of local foo {
  qui replace `xx' = 0 if step==0
}
sort shock step



gen zero = 0


egen ymax = rowmax(lrgdp* lmu* lplev*)
sum ymax
local ymax = .2*ceil(r(max)/.2)
egen ymin = rowmin(lrgdp* lmu* lplev*)
sum ymin
local ymin = .2*floor(r(min)/.2)
local ystep = .2

local title_lrgdp = "Output"
local title_lmu = "Markup"
local title_lplev "Price level"
local title_lpcom = "Commodity prices"

foreach xx in lrgdp lmu lplev lpcom {
   if "`xx'"=="lpcom" {
      local ymax = 3.2
      local ymin = -1.6
      local ystep = .8
   }

   #delim ;
   tw
   (rarea `xx'_p05_lmu_ces_yk_ums `xx'_p95_lmu_ces_yk_ums step, col("$c8%20") lc(white) lw(vvthin))
   (rarea `xx'_p05_lmu_cd `xx'_p95_lmu_cd step, col("$c7%20") lc(white) lw(vvthin))
   (line zero step, yaxis(1 2) lw(thin) lc(gs0))
   (line `xx'_lmu_cd step, yaxis(1 2) lw(medthick) lc("$c7"))
   (line `xx'_lmu_ces_yk_ums step, yaxis(1 2) lw(medthick) lc("$c8"))
   if shock=="money",
   yline(0, lc(gs0) lw(thin))
   ylab(`ymin'(`ystep')`ymax', axis(2) format(%-4.1f)) ymtick(`ymax', axis(2) nolab tlen(4.5))
   ylab(`ymin'(`ystep')`ymax', nolab) ymtick(`ymax', nolab tlen(4.5))
   xtitle("Quarters")
   xsca(r(-.5 20.5))
   xlabel(0(4)20, for(%3.0f)) xmtick(0(1)20)
   legend(order(4 "Cobb-Douglas" 5 "CES w/ variable utilization"))
   title("{bf:`title_`xx''}", nospan)
   name(`xx', replace)
   scheme(texpdf) xsize(3.24) ysize(2.7)
   ;
   #delim cr
   graph export ../../output/appendix/fig_irf_money_`xx'.pdf, replace

   cap drop ymax ymin
}

noi disp as text "$S_DATE $S_TIME  end fig_irf_money_all.do"

if "$S_CONSOLE"=="console" & "$batch"!="1" exit, STATA clear
