// appendix.do
//
// Master script for producing supplemental appendix results
//
// "The Cyclical Behavior of the Price-Cost Markup"
// By Christopher J. Nekarda and Valerie A. Ramey
//
// See instructions in project README.
//
// -------------------------------------------------------------------------- //

noi disp as text "$S_DATE $S_TIME  begin appendix.do"

// project-wide Stata settings
noi run ../_config.do


// run programs as part of master script? (0 = no, 1 = yes)
global batch = 1


// choose standard error for IRF
/* global ci = "nose"  // no SE */
/* global ci = ""  // use asymptotic SE */
global ci = "bs rep(400)"  // use bootstrapped SE


// filter for extracting cyclical components
global fil = "hp"
/* global fil = "bk" */
/* global fil = "fd" */


// robustness: alternative detrending and markup measures //
noi run fig_mu_robust.do
noi run tab_uncond_cyc_robust_fil.do
noi run tab_uncond_cyc_robust_meas.do

// compare to Gali, Gertler, Lopez-Salido (2007)
noi run comp_nr_ggls.do


// monetary SVAR robustness //
noi run svarmarkup_robust.do
noi run extract_irfs_robust.do

noi run fig_irf_money_all.do
noi run fig_irf_alt_order.do


// split-sample analysis and rolling regression //

// rolling elasticity of markup w.r.t. GDP
noi run fig_beta_roll.do

// unconditional cyclality over subsamples
noi run split_sample.do

// ratio of variance of shocks
noi run tab_shock_variance.do


// marginal markups //
noi run fig_dvdh.do
noi run fig_theta.do

noi run uncond_cyc_robust_marg.do


noi disp as text "$S_DATE $S_TIME  end appendix.do"

if "$S_CONSOLE" == "console" exit, STATA clear
