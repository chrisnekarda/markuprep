// split_sample.do
//
// Results reported in table B1
//
// "The Cyclical Behavior of the Price-Cost Markup"
// By Christopher J. Nekarda and Valerie A. Ramey
//
// See instructions in project README.
//
// -------------------------------------------------------------------------- //

// variable for date-time stamp
local d = string(clock("`c(current_date)' `c(current_time)'", "DMY hms"), "%tcCCYYNNDD!_HHMM")

cap log close
log using ../../log/split_sample_`d'.log, text replace

noi disp as text "$S_DATE $S_TIME  begin split_sample.do"

if "$fil" != "hp" & "$fil" != "bk" & "$fil" != "fd" {
   noi disp as error "Must specify a cyclical filter. See analysis.do"
   exit
}

use ../../data/processed/markupcyc_data.dta, clear


local varlist = "lrgdp lmu_cd"

noi disp as text "$S_DATE $S_TIME    extracting cyclical components (using $fil filter) for:"
foreach xx of local varlist {
   noi disp as text "$S_DATE $S_TIME    `xx' "
   if "$fil"=="hp" | "$fil"=="bk" {
      tsfilter $fil `xx'_cyc = `xx'
   }
   if "$fil"=="fd" {
      gen `xx'_cyc = D.`xx'
   }
   local fil = upper("$fil")
   label var `xx'_cyc "Cyclical component of `xx' (`fil')"
}


unab foo: *_cyc
foreach xx of local foo {
   qui replace `xx' = 100 * `xx'
}



// --------------- //
// TEST BREAKPOINT //
// --------------- //
reg lmu_cd_cyc lrgdp_cyc, nocons
noi estat sbsingle, nodots gen(swald)
local breakdate = r(breakdate)
gen byte late = qdate>=tq(`breakdate')


// ----- //
// table //
// ----- //

foreach xx in lmu_cd {
   ivreg2 `xx'_cyc lrgdp_cyc, bw(auto)
   eststo `xx'_full
   ivreg2 `xx'_cyc lrgdp_cyc if !late, bw(auto)
   eststo `xx'_H1
   ivreg2 `xx'_cyc lrgdp_cyc if late, bw(auto)
   eststo `xx'_H2
}

// report the table
noi disp _n "Elasticity of detrended markup w.r.t. detrended real GDP ($fil)"
noi esttab, keep(*lrgdp*) b(%4.2f) nose not star noobs compress nonumb


noi disp as text "$S_DATE $S_TIME  end split_sample.do"

cap log close

if "$S_CONSOLE"=="console" & "$batch"!="1" exit, STATA clear
