// fig_irf_alt_order.do
//
// Figures for CES markups
//
// "The Cyclical Behavior of the Price-Cost Markup"
// By Christopher J. Nekarda and Valerie A. Ramey
//
// See instructions in project README.
//
// -------------------------------------------------------------------------- //

noi disp as text "$S_DATE $S_TIME  begin fig_irf_alt_order.do"

use ../../output/appendix/oirf.dta, clear

// only plotting these two measures
keep if markup == "lmu_cd" | markup == "lmu_ces_yk_ums"

replace markup = "cd_" if markup=="lmu_cd"
replace markup = "ces_" if markup=="lmu_ces_yk_ums"

// scale for humans (multiply by 100)
unab foo: lmu* lrgdp*
foreach xx of local foo {
   replace `xx' = 100 * `xx'
   rename `xx' `xx'_
}

drop line lpcom* lplev* *se
reshape wide lmu* lrgdp*, i(shock step) j(markup) s

// reshape on the shock
reshape wide lmu* lrgdp*, i(step) j(shock) s

// make the stata shocks start in period 1
expand 2, gen(exp)
drop if exp == 1 & step > 0
replace step = step + 1 if exp == 0
drop if step > 20
drop exp
unab foo: lmu* lrgdp*
foreach xx of local foo {
   qui replace `xx' = 0 if step == 0
}
sort step

// find max and min
egen ymax = rowmax(lrgdp* lmu*)
sum ymax
local ymax = .2 * ceil(r(max) / .2)
egen ymin = rowmin(lrgdp* lmu*)
sum ymin
local ymin = .2 * floor(r(min) / .2)

gen byte zero = 0


foreach xx in cd ces {
   #delim ;
   tw
   (rarea lrgdp_p05_`xx'_money_alt lrgdp_p95_`xx'_money_alt step, col("$c8%20") lc(white) lw(vvthin))
   (rarea lrgdp_p05_`xx'_money lrgdp_p95_`xx'_money step, col("$c7%20") lc(white) lw(vvthin))
   (line zero step, yaxis(1 2) lw(thin) lc(gs0))
   (line lrgdp_`xx'_money_alt step, yaxis(1 2) lw(medthick) lc("$c8"))
   (line lrgdp_`xx'_money step, yaxis(1 2) lw(medthick) lc("$c7")),
   yline(0, lc(gs0) lw(thin))
   ylab(`ymin'(.2)`ymax', axis(2) format(%-4.1f)) ymtick(`ymax', axis(2) nolab tlen(4.5))
   ylab(`ymin'(.2)`ymax', nolab) ymtick(`ymax', nolab tlen(4.5))
   xtitle("Quarters")
   xsca(r(-.5 20.5))
   xlabel(0(4)20, for(%3.0f)) xmtick(0(1)20)
   legend(order(5 "{&mu} ordered last" 4 "{it:r} ordered last")) /* pos(7) bm(4 0 4 0))*/
   title("{bf:Output}", nospan)
   name(output, replace)
   scheme(texpdf) xsize(3.24) ysize(2.7)
   ;
   #delim cr
   graph export ../../output/appendix/fig_irf_money_`xx'_output.pdf, replace

   #delim ;
   tw
   (rarea lmu_p05_`xx'_money_alt lmu_p95_`xx'_money_alt step, col("$c8%20") lc(white) lw(vvthin))
   (rarea lmu_p05_`xx'_money lmu_p95_`xx'_money step, col("$c7%20") lc(white) lw(vvthin))
   (line zero step, yaxis(1 2) lw(thin) lc(gs0))
   (line lmu_`xx'_money_alt step, yaxis(1 2) lw(medthick) lc("$c8"))
   (line lmu_`xx'_money step, yaxis(1 2) lw(medthick) lc("$c7")),
   yline(0, lc(gs0) lw(thin))
   ylab(`ymin'(.2)`ymax', axis(2) format(%-4.1f)) ymtick(`ymax', axis(2) nolab tlen(4.5))
   ylab(`ymin'(.2)`ymax', nolab) ymtick(`ymax', nolab tlen(4.5))
   xtitle("Quarters")
   xsca(r(-.5 20.5))
   xlabel(0(4)20, for(%3.0f)) xmtick(0(1)20)
   legend(order(5 "{&mu} ordered last" 4 "{it:r} ordered last")) /* pos(7) bm(4 0 4 0))*/
   title("{bf:Markup}", nospan)
   name(markup, replace)
   scheme(texpdf) xsize(3.24) ysize(2.7)
   ;
   #delim cr
   graph export ../../output/appendix/fig_irf_money_`xx'_markup.pdf, replace
}


noi disp as text "$S_DATE $S_TIME  end fig_irf_alt_order.do"

if "$S_CONSOLE"=="console" & "$batch"!="1" exit, STATA clear
