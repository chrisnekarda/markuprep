// fig_beta_roll.do
//
// Figures for alternative markups - robustness
//
// "The Cyclical Behavior of the Price-Cost Markup"
// By Christopher J. Nekarda and Valerie A. Ramey
//
// See instructions in project README.
//
// -------------------------------------------------------------------------- //

noi disp as text "$S_DATE $S_TIME  begin fig_beta_roll.do"

use ../../data/interim/markups_cyc_hp.dta, clear


// full-sample regression
ivreg2 lmu_cd_cyc lrgdp_cyc, bw(auto)
eststo mu_full
local beta_full = _b[lrgdp_cyc]
local se_full = _se[lrgdp_cyc]
local bw = e(bw)


// rolling regression
rolling _b _se, window(40) sav(roll.dta, replace): ivreg2 lmu_cd_cyc lrgdp_cyc, bw(auto)

use roll, clear
/* rename end qdate */
gen int qdate = (start+end)/2 + .5
tsset qdate, q

gen beta = _b_lrgdp_cyc
gen beta_p5 = beta - invnormal(0.025)*_se_lrgdp_cyc
gen beta_p95 = beta + invnormal(0.025)*_se_lrgdp_cyc
gen se = _se_lrgdp_cyc

set obs `=_N+1'
replace qdate=tq(1947q1) in l
set obs `=_N+1'
replace qdate=tq(2017q4) in l
tsfill

// draw chart
gen beta_full = `beta_full'
gen se_full = `se_full'
gen zero = 0

#delim ;
tw
(rarea beta_p5 beta_p95 qdate, col("$c7%25") lc(white) lw(vvthin))
(line zero qdate, yaxis(1 2) lw(medthin) lc(gs0))
(line beta_full qdate, yaxis(1 2) lw(medthin) lc("$c8") lp(dash))
(line beta qdate, yaxis(1 2) lw(medthick) lc("$c7"))
,
ttitle("")
subtitle("Elasticity", pos(11))
legend(order(4 "Rolling regression" 3 "Full sample") bm(4 0 0 2))
ylab(-1.0(.2)1.0, for(%-6.1f)) ymtick(1, nolab tlen(4.5))
ylab(-1.0(.2)1.0, axis(2) for(%-6.1f)) ymtick(1, axis(2) nolab tlen(4.5))
yline(0, lc(gs0) lw(thin))
xtick(`=tq(1947q1)-4.5'(4)`=tq(2017q4)+4.5')
xlab(`=tq(1950q3)-.5'(20)`=tq(2015q3)-.5', notick for(%tqCCYY))
xline(
`=tq(1948q4)-.5'(.25)`=tq(1949q4)-.5'
`=tq(1953q3)-.5'(.25)`=tq(1954q2)-.5'
`=tq(1957q3)-.5'(.25)`=tq(1958q2)-.5'
`=tq(1960q2)-.5'(.25)`=tq(1961q1)-.5'
`=tq(1969q4)-.5'(.25)`=tq(1970q4)-.5'
`=tq(1973q4)-.5'(.25)`=tq(1975q1)-.5'
`=tq(1980q1)-.5'(.25)`=tq(1980q3)-.5'
`=tq(1981q3)-.5'(.25)`=tq(1982q4)-.5'
`=tq(1990q3)-.5'(.25)`=tq(1991q1)-.5'
`=tq(2001q1)-.5'(.25)`=tq(2001q4)-.5'
`=tq(2007q4)-.5'(.25)`=tq(2009q2)-.5'
, lc("$c16") lw(medium))
scheme(texpdf)
name(beta, replace)
;
#delim cr
graph export ../../output/appendix/fig_beta_roll.pdf, replace

erase roll.dta


noi disp as text "$S_DATE $S_TIME  end fig_beta_roll.do"

if "$S_CONSOLE"=="console" & "$batch"!="1" exit, STATA clear
