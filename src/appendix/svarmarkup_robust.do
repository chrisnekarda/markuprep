// svarmarkup_robust.do
//
// Results reported in table 2: Conditional cyclicality
//
// "The Cyclical Behavior of the Price-Cost Markup"
// By Christopher J. Nekarda and Valerie A. Ramey
//
// See instructions in project README.
//
// -------------------------------------------------------------------------- //

// draw plots? (1 = yes, 0 = no)
local plot = 0

// save IRF files? (1 = yes, 0 = no)
local save_irf = 1

set more off
est drop _all

// variable for date-time stamp
local d = string(clock("`c(current_date)' `c(current_time)'", "DMY hms"), "%tcCCYYNNDD!_HHMM")

cap log close
log using ../../log/svarmarkup_robust_`d'.log, text replace

noi disp as text "$S_DATE $S_TIME  begin svarmarkup_robust.do"


use ../../data/processed/markupcyc_data.dta, clear


// ---------------------------------------------- //
// ESTIMATE THE STRUCTURAL VECTOR AUTOREGRESSIONS //
// ---------------------------------------------- //

// create variables needed for SVARs
gen t = _n
gen t2 = t^2

//gen lrgdp = ln(rgdp)
gen lrgdpc = ln(rgdp / pop)
gen lrgovc = ln(rgov / pop)
gen lpcom = ln(pzall)
gen lp = ln(pgdp)

// switch sign so positive fed funds shock is expansionary
replace rff = -rff
replace ssr = -ssr
// replace ssr with actual funds rate outside the ZLB period
replace ssr = rff if tin(, 2008q4)
replace ssr = rff if tin(2016q4, )

// IST shocks //
gen lpe = ln(ppcendsv / pequip)
label var lpe "log ratio price of PCE ND+SV to price of equipment investment"

gen infl = 4 * ln(pgdp / L.pgdp)


local varlist = "lmu_cd lmu_ces_yk_ums"

foreach xx of local varlist {

   // ---------------------------------------------- //
   // MONETARY POLICY SHOCKS
   // CEE monetary VAR w/ SSR -- ALTERNATIVE ORDERING w/ FUNDS RATE LAST
   // fed funds is defined as its negative so a positive shock is expansionary

   noi var lrgdpc lp lpcom `xx' ssr, lags(1/4) exog(t t2)
   noi irf create moneyvar_alt, step(20) $ci set(../../est/irf_robust_`xx', replace)
   noi irf table oirf, level(90) impulse(ssr) response(lrgdpc `xx')

   if `plot' {
      irf graph oirf, impulse(ssr) response(lrgdpc) ustep(20) plot1opt(col(navy)) level(90) ci1opts(lc(white) lw(vvthin) fc(navy%20)) xlab(0(4)20, tlen(*2)) xmtick(0(1)20, tlen(*2)) xtitle("Quarters") yline(0, lc(gs0) lw(thin)) scheme(s1color) ysize(2.5) xsize(3.25) graphr(m(medsmall)) title("{bf:Output}") name(output, replace) byopts(legend(off) note(""))
      irf graph oirf, impulse(ssr) response(`xx') ustep(20) plot1opt(col(navy)) level(90) ci1opts(lc(white) lw(vvthin) fc(navy%20)) xlab(0(4)20, tlen(*2)) xmtick(0(1)20, tlen(*2)) xtitle("Quarters") yline(0, lc(gs0) lw(thin)) scheme(s1color) ysize(2.5) xsize(3.25) graphr(m(medsmall)) title("{bf:Markup}") name(markup, replace) byopts(legend(off) note(""))
      graph combine output markup, row(1) scheme(s1color) xsize(6.5) ysize(2.5) title("{bf:CEE VAR w/ SSR - `xx'}") note("Shaded area is 90-percent confidence interval.")
      graph export ../../output/appendix/irf_moneyvar_alt.pdf, replace
   }

   // Extract estimated shocks
   // SSR is ordered last so you must condition on current values of other
   // variables (including the markup)
   reg ssr L(0/4).lrgdpc L(0/4).lp L(0/4).lpcom L(0/4).`xx' L(1/4).ssr t t2
   predict money_shock_alt_`xx', resid

}

/* save ../../data/interim/svarmarkup_robust.dta, replace */

preserve
keep qdate *shock*

reshape long money_shock_alt_, i(qdate) j(markup) string

label var markup "Markup measure"

rename money_shock_alt_ money_shock_alt
label var money_shock_alt "Estimated monetary policy shock (alternate ordering)"

compress
sort markup qdate
label data "Estimated shocks from 'The Cyclical Behavior of the Price-Cost Markup'"
save ../../output/appendix/estimated_shocks_robust.dta, replace

noi des, f
restore


noi disp as text "$S_DATE $S_TIME  end svarmarkup_robust.do"

cap log close

if "$S_CONSOLE"=="console" & "$batch"!="1" exit, STATA clear
