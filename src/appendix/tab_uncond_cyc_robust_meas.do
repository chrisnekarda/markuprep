// tab_uncond_cyc_robust_meas.do
//
// Results reported in table A2: Unconditional cyclicality - Robustness (II)
//
// "The Cyclical Behavior of the Price-Cost Markup"
// By Christopher J. Nekarda and Valerie A. Ramey
//
// See instructions in project README.
//
// -------------------------------------------------------------------------- //

est drop _all

// variable for date-time stamp
local d = string(clock("`c(current_date)' `c(current_time)'", "DMY hms"), "%tcCCYYNNDD!_HHMM")

cap log close
log using ../../log/tab_uncond_cyc_robust_meas_`d'.log, text replace

noi disp as text "$S_DATE $S_TIME  begin tab_uncond_cyc_robust_meas.do"


use ../../data/processed/markupcyc_data.dta, clear



// optional start and ending dates for all variables, estbeg will not affect
// the variables with a shorter sample
local estbeg = tq(1947q1)
local estend = tq(2017q4)

local varlist = "lrgdp lmu_cd lmu_cd_nfb lmu_cd_nfcb"

foreach fil in hp bk fd {
   noi disp as text "$S_DATE $S_TIME    extracting cyclical components (using `fil' filter) for:"
   local i 0
   foreach xx of local varlist {
      noi disp as text "$S_DATE $S_TIME    `xx' "
      if "`fil'"=="hp" | "`fil'"=="bk" {
         tsfilter `fil' `xx'_`fil' = `xx'
      }
      if "`fil'"=="fd" {
         gen `xx'_`fil' = D.`xx'
      }
      local FIL = upper("`fil'")
      label var `xx'_`fil' "Cyclical component of `xx' (`FIL')"

      if "`xx'"!="lrgdp" {
         local i `++i'
         ivreg2 `xx'_`fil' lrgdp_`fil' if qdate>=`estbeg' & qdate<=`estend', bw(auto)
         eststo est`i'_`fil'
      }
   }
}

use ../../data/processed/markupcyc_data.dta, clear
keep qdate lmu_cd lrgdp


// ------ //
// annual //
// ------ //
gen int year = year(dofq(qdate))
collapse lrgdp lmu*, by(year)

merge 1:m year using ../../data/interim/dleu_markup, nogen keepusing(lmu_cogs)
replace lmu_cogs = lmu_cogs / 100
drop if year==.
tsset year, y

local varlist = "lrgdp lmu_cd lmu_cogs"

foreach fil in hp bk fd {
   noi disp as text "$S_DATE $S_TIME    extracting cyclical components (using `fil' filter) for:"
   local i 3
   foreach xx of local varlist {
      noi disp as text "$S_DATE $S_TIME    `xx' "
      if "`fil'"=="hp" | "`fil'"=="bk" {
         tsfilter `fil' `xx'_`fil' = `xx'
      }
      if "`fil'"=="fd" {
         gen `xx'_`fil' = D.`xx'
      }
      local FIL = upper("`fil'")
      label var `xx'_`fil' "Cyclical component of `xx' (`FIL')"

      if "`xx'"!="lrgdp" {
         local i `++i'
         ivreg2 `xx'_`fil' lrgdp_`fil', bw(auto)
         eststo est`i'_`fil'
      }
   }
}


// construct a list of variables as numbered in the table
local tablist
forvalues i = 1/5 {
   foreach fil in hp bk fd {
      local tablist = "`tablist' est`i'_`fil'"
   }
}

// re-arrange estout matrixes to produce a table by rows
qui esttab `tablist', se nostar r2 drop(_cons)
matrix C = r(coefs)
eststo clear
local rnames : rownames C
local models : coleq C
local models : list uniq models
local i 0
foreach name of local rnames {
   local ++i
   local j 0
   capture matrix drop b
   capture matrix drop se
   foreach model of local models {
      local ++j
      matrix tmp = C[`i', 2 * `j' - 1]
      if tmp[1, 1]<. {
            matrix colnames tmp = `model'
            matrix b = nullmat(b), tmp
            matrix tmp[1, 1] = C[`i', 2 * `j']
            matrix se = nullmat(se), tmp
      }
   }
   ereturn post b
   quietly estadd matrix se
   eststo `name'
}

// report the table
noi disp _n "Elasticity of detrended markup w.r.t. detrended real GDP"
noi esttab, b(%4.2f) wide se star mtitle noobs compress nonumb

noi disp as text "$S_DATE $S_TIME  end tab_uncond_cyc_robust_meas.do"

cap log close

if "$S_CONSOLE"=="console" & "$batch"!="1" exit, STATA clear
