// comp_nr_ggls.do
//
// Comaprison of Nekarda and Ramey results with those of Gali, Gertler, and
// Lopez-Salido (2007)
//
// "The Cyclical Behavior of the Price-Cost Markup"
// By Christopher J. Nekarda and Valerie A. Ramey
//
// See instructions in project README.
//
// Findings:
//  1. GGLS, RW approximations for CES are very close to our direct measure.
//       In fact, for their case that do not allow variable capital utilization,
//       GGLS markup is a little less countercyclical than ours.
//       Our key innovation is allowing for variable capital utilization.
//
//  2. GGLS overhead labor vs. ours:  The key difference is the assumption on the
//      steady-state ratio of overhead to variable labor
// -------------------------------------------------------------------------- //

est drop _all
if "$S_CONSOLE" == "console" set linesize 150  // so the tables don't wrap

// variable for date-time stamp
local d = string(clock("`c(current_date)' `c(current_time)'", "DMY hms"), "%tcCCYYNNDD!_HHMM")

cap log close
log using ../../log/comp_nr_ggls_`d'.log, text replace

noi disp as text "$S_DATE $S_TIME  begin comp_nr_ggls.do"


use ../../data/processed/markupcyc_data.dta, clear


*******************************************************************************
* 1. CES ADJUSTMENT COMPARISON TO GGLS
*******************************************************************************

// A. CREATE KEY VARIABLES //

// CES elasticity
scalar sigma = 0.5

// GGLS calibration
gen lmu_ces_yk_ggls = lmu_cd -0.4 * ln(yk_bus)

// Compare adjustment terms added to log CD baseline markup

// NR Y/K measure with constant capital utilization
gen ces_term_nr = ln(1 - alphak * yk_bus^(1/sigma - 1))

// GGLS adjustment
gen ces_term_ggls = -0.4 * ln(yk_bus)



// B. HP FILTER AND ESTIMATE ELASTICITY //
local estbeg = tq(1947q1)
local estend = tq(2017q4)

// Sample 1947:Q1 to 2017:Q4
local varlist = "ces_term_nr ces_term_ggls "

tsfilter hp ly_hp = lrgdp if qdate>=`estbeg' & qdate<=`estend'

foreach xx of local varlist {
   // extract cyclical component
   tsfilter hp `xx'_hp = `xx' if qdate>=`estbeg' & qdate<=`estend'

   // estimate elasticity
   ivreg2 `xx'_hp ly_hp if qdate>=`estbeg' & qdate<=`estend', bw(auto)
   eststo `xx'_hp0
}


noi disp _n "ESTIMATES FROM " %tq `estbeg' " TO " %tq `estend'
noi esttab *hp0, b(%5.2f) se star keep(*ly*) model(18)


// C.  HOW GOOD IS THE GGLS APPROXIMATION? //

gen line45 = ces_term_nr_hp

gen ces_term_ggls_hp_pre95 = ces_term_ggls_hp if qdate<q(1995q1)
gen ces_term_ggls_hp_post95 = ces_term_ggls_hp if qdate>=q(1995q1)

label var ces_term_nr_hp "NR adjustment"
label var ces_term_ggls_hp "GGLS approximation"
label var ces_term_ggls_hp_pre95 "GGLS approx. to pre-1995 data"
label var ces_term_ggls_hp_post95 "GGLS approx. to post-1995 data"

label var line45 "45 degree line"

#delim ;
tw scatter
line45 ces_term_ggls_hp_pre95 ces_term_ggls_hp_post95 ces_term_nr_hp,
c(l . .)
ms(i o d)
mcolor(. red blue)
ytitle("GGLS adjustment")
xtitle("NR adjustment")
title("How Good is GGLS CES Approximation?")
name(ces, replace)
;
#delim cr


// D. COMPARE THE VARIOUS LOG MARKUP ELASTICITIES //

// HP filter and estimate elasticity to output
local varlist = "lmu_cd lmu_cd_ws lmu_ces_yl_zsvar lmu_ces_yk lmu_ces_yk_ggls "

foreach xx of local varlist {
   // extract cyclical component
   tsfilter hp `xx'_hp = `xx' if qdate>=`estbeg' & qdate<=`estend'

   // estimate elasticity
   ivreg2 `xx'_hp ly_hp if qdate>=`estbeg' & qdate<=`estend', bw(auto)
   eststo `xx'_hp1
}

drop ly_hp line45

noi disp _n "ESTIMATES FROM " %tq `estbeg' " TO " %tq `estend'
noi esttab *`fil'1, b(%5.2f) se star keep(*ly*) model(18)



*******************************************************************************
* 2. OVERHEAD LABOR ADJUSTMENT COMPARISON TO GGLS
*******************************************************************************

/* BACKGROUND AND VARIABLE DEFINITIONS

 GGLS log markup = - ln(s(t)) - delta_ggls*(total hours deviation from trend), delta_ggls = overhead-to-variable labor, calibrated to 0.4

 NR log markup = - ln(s(t)) - ln(variable hours/total hours) - ln(wages and salaries/total comp)

 nu = ratio_emp_ces = emp_priv_prod / emp_priv
*/


// A. COMPARE ADJUSTMENT TERMS ADDED TO LOG CD BASELINE MARKUP //

// Calculate average GGLS delta using our data

gen delta = (emp_priv - emp_priv_prod)/emp_priv_prod

noi sum delta

// log production workers
gen lemp_priv_prod = ln(emp_priv_prod)
// log nonproduction workers
gen lemp_priv_nonprod = ln(emp_priv - emp_priv_prod)


// Create the various terms added to baseline log markup

// GGLS version
gen oh_term_ggls = -0.4 * ln(hours_bus)

// GGLS approx, with NR delta calibration
gen oh_term_ggls_b = -0.22 * ln(hours_bus)

// create series on overhead hours
gen hours_oh_bus = (1 - nu) * hours_bus

// GGLS approximation and delta
gen lmu_oh_ggls = lmu_cd - 0.4 * ln(hours_bus)

// GGLS approximation, NR delta calibration
gen lmu_oh_ggls_b = lmu_cd - 0.22 * ln(hours_bus)

// Full approx., NR delta calibration
gen lmu_oh_ggls_c = lmu_cd - 0.22 * (ln(hours_bus) - ln(hours_oh_bus))


// HP filter and estimate elasticity to output
local varlist = "lmu_cd_ws_oh lmu_oh_ggls lmu_oh_ggls_b lmu_oh_ggls_c lemp_priv_prod lemp_priv_nonprod"
local estbeg = tq(1964q1)
local estend = tq(2017q4)

// re-do GDP over shorter sample
cap drop ly_hp
tsfilter hp ly_hp  = lrgdp if qdate>=`estbeg' & qdate<=`estend'

foreach xx of local varlist {
   // extract cyclical component
   tsfilter hp `xx'_hp = `xx' if qdate>=`estbeg' & qdate<=`estend'

   // estimate elasticity
   ivreg2 `xx'_hp ly_hp if qdate>=`estbeg' & qdate<=`estend', bw(auto)
   eststo `xx'_hp2
}

noi disp _n "ESTIMATES FROM " %tq `estbeg' " TO " %tq `estend'
noi esttab *hp2, b(%5.2f) se star keep(*ly*) model(18)

noi disp as text "$S_DATE $S_TIME  end comp_nr_ggls.do"

cap log close

if "$S_CONSOLE" == "console" & "$batch" != "1" exit, STATA clear
