// uncond_cyc_robust_marg.do
//
// Results for unconditional cyclicality using marginal markup
//
// "The Cyclical Behavior of the Price-Cost Markup"
// By Christopher J. Nekarda and Valerie A. Ramey
//
// See instructions in project README.
//
// -------------------------------------------------------------------------- //

est drop _all

// variable for date-time stamp
local d = string(clock("`c(current_date)' `c(current_time)'", "DMY hms"), "%tcCCYYNNDD!_HHMM")

cap log close
log using ../../log/uncond_cyc_robust_marg_`d'.log, text replace

noi disp as text "$S_DATE $S_TIME  begin uncond_cyc_robust_marg.do"

use ../../data/interim/markups_cyc_hp, clear

ivreg2 lmu_cd_cyc lrgdp_cyc if lmu_cd_marg_cyc != ., bw(auto)
eststo lmu_cd

ivreg2 lmu_cd_marg_cyc lrgdp_cyc, bw(auto)
eststo lmu_cd_marg

noi esttab, keep(lrgdp_cyc) b(%4.2f) se star nonumb


noi disp as text "$S_DATE $S_TIME  end uncond_cyc_robust_marg.do"

cap log close

if "$S_CONSOLE"=="console" & "$batch"!="1" exit, STATA clear
