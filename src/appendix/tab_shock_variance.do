// tab_shock_variance.do
//
// Results reported in table B2: Ratio of Variances of Estimated Shocks
//
// "The Cyclical Behavior of the Price-Cost Markup"
// By Christopher J. Nekarda and Valerie A. Ramey
//
// See instructions in project README.
//
// -------------------------------------------------------------------------- //


// variable for date-time stamp
local d = string(clock("`c(current_date)' `c(current_time)'", "DMY hms"), "%tcCCYYNNDD!_HHMM")

cap log close
log using ../../log/tab_shock_variance_`d'.log, text replace

noi disp as text "$S_DATE $S_TIME  begin tab_shock_variance.do"

use ../../output/estimated_shocks.dta, clear

// use baseline C-D markup
keep if markup == "lmu_cd"

// compute variance of shocks 1995--2017 to 1947--1994
matrix B1 = J(1, 4, 0)
local i = 0
foreach xx in money govmil tfp ist {
   local i = `++i'
   summ `xx'_shock if qdate < tq(1995q1)
   scalar `xx'_var0 = r(Var)
   summ `xx'_shock if qdate >= tq(1995q1)
   scalar `xx'_var1 = r(Var)
   mat B1[1,`i'] = `xx'_var1/`xx'_var0
}

mat colnames B1 = "money" "govmil" "tfp" "ist"

// report the table values
noi disp _n "Ratio of Variances of Estimated Shocks"
noi mat list B1, for(%4.2f)

noi disp as text "$S_DATE $S_TIME  end tab_shock_variance.do"

cap log close

if "$S_CONSOLE"=="console" & "$batch"!="1" exit, STATA clear
