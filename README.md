# Data and programs for "The Cyclical Behavior of the Price-Cost Markup"

[**The Cyclical Behavior of the Price-Cost Markup**](https://onlinelibrary.wiley.com/journal/15384616)\
[Christopher J. Nekarda](https://chrisnekarda.com/) and [Valerie A. Ramey](https://econweb.ucsd.edu/~vramey/)\
*Journal of Money, Credit and Banking*, Vol. 00, No. 0 (XXXX 2021).

Updated July 2020

#### Downloads

* Main paper - [Published](https://onlinelibrary.wiley.com/journal/15384616) | [Preprint (PDF)](https://chrisnekarda.com/papers/markupcyc.pdf)
* Supplementary appendix - [Published](https://onlinelibrary.wiley.com/journal/15384616) | [Preprint (PDF)](https://chrisnekarda.com/papers/markupcyc_appendix.pdf)
* Replication files - [Programs and data (zip)](https://gitlab.com/chrisnekarda/markuprep/-/raw/master/markupcyc_replication.zip) | [Source code only (GitLab)](https://gitlab.com/chrisnekarda/markuprep)

## General information

* The [replication archive](https://gitlab.com/chrisnekarda/markuprep/-/raw/master/markupcyc_replication.zip) includes all necessary data files and programs, as well as the output of those programs.
  * The master script for producing the main paper's results is `src/analysis/analysis.do`; see the [main paper](README.md#main-paper) section of the replication instructions below.
  * The [data](README.md#data) section of the replication instructions below describes how to reconstruct or update the data.
* Programs are written for Stata (v15) and have been tested under Windows and Linux; not all programs run under Windows. Programs are intended to be executed using `run` (rather than `do`). In a few cases, executing with `do` will cause the program to fail; these are noted in the program itself.
* The master scripts must be run from their associated subdirectory, and can be run either interactively or in batch mode. For example, to reproduce the tables and figures for the main paper from an interactive Stata session, type

  ```Stata
  cd [/path/to/main/directory/]src/analysis
  run analysis.do
  ```

  To run in batch mode (in macOS/Linux), type

  ```shell
  cd [/path/to/main/directory/]src/analysis
  stata -q run analysis.do
  ```

### Project directory structure

```
.
├── README.md        <-- this file
├── data             <-- data files
│   ├── interim      <-- intermediate data produced during processing
│   ├── processed    <-- processed data for analysis
│   └── raw          <-- original source data files
├── est              <-- estimation results (e.g., .est, .irf)
├── log              <-- log files
├── output           <-- output for main text tables and figures
│   └── appendix     <-- output for supplementary appendix
└── src              <-- source code
    ├── analysis     <-- main paper calculations, tables, and figures
    ├── appendix     <-- supplementary appendix calculations, tables, and figures
    ├── data         <-- data construction
    └── external     <-- external dependencies (e.g., packages added to base Stata)
```

## Replication instructions

The steps below will reproduce the paper's results in **macOS/Linux**. Additional details are provided in the sections below.
**Windows** users, see the detailed instructions below.

```shell
mkdir markuprep
cd markuprep
wget https://gitlab.com/chrisnekarda/markuprep/-/raw/master/markupcyc_replication.zip
unzip markupcyc_replication.zip

cd src/analysis
stata -q run analysis.do
cd ../..

cd src/appendix
stata -q run appendix.do
cd ../..
```

### Main paper

1. Download and extract replication [programs and data](https://gitlab.com/chrisnekarda/markuprep/-/raw/master/markupcyc_replication.zip)
1. In Stata, change to the `src/analysis` directory
1. Run `analysis.do`

### Supplemental appendix

1. Follow steps above to replicate the [main paper](README.md#main-paper). This creates some of the data files needed for the supplemental appendix.
1. In Stata, change to the `src/appendix` directory
1. Run `appendix.do`

### Data

#### To rebuild the data files using existing source data

1. In Stata, change to the `src/data` directory
1. Run `build_data.do`

#### To download new data or otherwise change source data

1. Edit `src/data/download_data.do` and change `vintage(21jun2019)` (line 23 and line 41) to the desired date (Caution: non-FRED data do not respect this vintage date)
1. In Stata, change to the `src/data` directory
1. Run `download_data.do`
1. Run `build_data.do`

Note: Parts of `src/data/download_data.do` require command line tools that are only available by default in macOS/Linux; the program will not download some data under Windows without modifications.
